
%-------------------------------------------------------------------------
%----  This file contains the input parameters needed at run-time for 
%       simulations. It is based on and closely resembles the GADGET-3
%       parameterfile (format of which and parsing routines written by 
%       Volker Springel [volker.springel@h-its.org]). It has been updated
%       with new naming conventions and additional variables as needed by 
%       Phil Hopkins [phopkins@caltech.edu] for GIZMO.
%-------------------------------------------------------------------------

% based on 2030/SN_test_high_rho_floor_30_thinner_Q2_cut.param

%----  Relevant files
InitCondFile  ../ICs/large_scale_0m1_thinner_Q2.dat
OutputDir     longrun_medflow_vesc_defaultaniso_polar/

%---- File formats 
ICFormat    1  % 1=binary, 3=hdf5, 4=cluster 
SnapFormat  3  % 1=binary, 3=hdf5 

%---- Output parameters 
RestartFile                 restart 
SnapshotFileBase            snapshot 
OutputListOn                0  % =1 to use list in "OutputListFilename" 
OutputListFilename          output_times.txt  % list of times for snaps 
NumFilesPerSnapshot         1 
NumFilesWrittenInParallel   1  % must be < N_processors & power of 2

%---- Output frequency 
TimeOfFirstSnapshot     0.0 
TimeBetSnapshot         1.e-5
TimeBetStatistics       0.0001 

%---- CPU-time limits 
TimeLimitCPU            5000000  % in seconds 
CpuTimeBetRestartFile   3600 	% in seconds 
ResubmitOn              0 
ResubmitCommand         my-scriptfile 

%----- Memory allocation
%MaxMemSize          2500    % sets maximum MPI process memory use in MByte 
MaxMemSize          10000    % sets maximum MPI process memory use in MByte 
PartAllocFactor     30.0     % memory load allowed for better cpu balance 
BufferSize          400     % in MByte 

%---- Characteristics of run 
TimeBegin   0.0    % Beginning of the simulation 
TimeMax     5.e-3     % End of the simulation 
%TimeMax     .005     % End of the simulation 

%---- Cosmological parameters 
ComovingIntegrationOn   0       % is it cosmological? (yes=1, no=0)
BoxSize                 1.  % in code units
Omega0                  0    % =0 for non-cosmological
OmegaLambda             0    % =0 for non-cosmological
OmegaBaryon             0   % =0 for non-cosmological
HubbleParam             1     % little 'h'; =1 for non-cosmological runs

%---- Accuracy of time integration 
MaxSizeTimestep         1.0e-6   % in code units, set for your problem
MinSizeTimestep         1.0e-12 % set this very low, or risk stability

%---- Tree algorithm, force accuracy, domain update frequency 
TreeDomainUpdateFrequency   0.005	% 0.0005-0.05, dept on core+particle number  

%---- System of units 
UnitLength_in_cm            3.085678e21     % 1.0 kpc/h
UnitMass_in_g               1.989e43  	    % 1.0e10 solar masses/h
UnitVelocity_in_cm_per_s    1.0e5   	    % 1 km/sec
GravityConstantInternal     0		        % calculated by code if =0

%---- Initial temperature & temperature floor 
InitGasTemp     1000.	    % set by IC file if =0 
MinGasTemp      30.	    % don't set <10 in explicit feedback runs, otherwise 0

%---- soton AGN parameters
MaxGasTemp      1.e8    % to avoid superheating
AGN_M           1.e6     % mass of SMBH
AGN_Edd         0.01    % Eddington ratio, used to calculate AGN lumunosity
AGN_TSput       1.e5     % (K) temperature at which dust magically disappears
dtHeatFac       1.       % max factor of heating per timestep
dtCoolFac       0.1        % max factor of cooling per timestep
AGNPotentialSoft        1.e-2     % pc, softening for SMBH grav so particles don't explode
HernquistBulgeMass 1.e9    % hernquist bulge mass in solar masses
HernquistBulgeScale 250.   % hernquist bulge scale length in pc
AGNTabFile      ../cooling_tables/shrunk_table_060319_m0.1_hsmooth_tau.dat
AGNTabLabelFile ../cooling_tables/shrunk_table_labels_060319tau.dat
AGNDustlessTabFile      ../cooling_tables/shrunk_table_060319_m0.1_hsmooth_taunodust.dat
AGNDustlessTabLabelFile ../cooling_tables/shrunk_table_labels_060319taunodust.dat
AGNHighdenseTabFile      ../cooling_tables/shrunk_table_060319_m0.1_hsmooth_taudense.dat
AGNHighdenseTabLabelFile ../cooling_tables/shrunk_table_labels_060319taudense.dat
inflowRate     0.
inflowRad      100.
innerKillRad   0.5
AGN_theta      0.

%---- soton AGN outflow and intrinsic optical depth parameters
intrinsic_tau0              5.

% in degrees
intrinsic_tau_transition    40.
intrinsic_index0            10.
intrinsic_index1            50.5
AGN_isotropy   1.e2

outflowRate                 0.378
outflowRad                  1.
outflowThetaCentre          35.
outflowThetaWidth           50.
outflowVel                  100.
outflowCircVel              65.7
outflowSourceRad            0.015


%---- soton SF parameters
%imposed_SNR     5.e3     % SN per Myr
imposed_SNR     1.e-9      % SN per Myr
SN_energy       .1        % in 1e51 erg units
SN_mass         1000.     % in Msun
SN_thermal_frac 0.
%SN_temp         1.e6      % in K
SN_scalerad     7.        % in pc
%FUV_factor      1000.    % in Habing units
thresholdSFDensity 1.e10

%---- Density/volume estimation (kernel) 
DesNumNgb               32      % 32 for standard kernel, 60-114 for quintic 
MaxHsml                 1.0e10  % minimum gas kernel length (some very large value to prevent errors)
MinGasHsmlFractional    1       % minimum kernel length relative to gas force softening (<= 1)

AGS_DesNumNgb           32  % neighbor number for calculating adaptive gravsoft

%---- Gravitational softening lengths 
%----- Softening lengths per particle type. If ADAPTIVE_GRAVSOFT is set, these  
%-------- are the minimum softening allowed for each type ------- 
%-------- (units are co-moving for cosmological integrations)
SofteningGas    1.e-7    % gas (type=0) (in kpc units)
SofteningHalo   0.020    % dark matter/collisionless particles (type=1)
SofteningDisk   0.150    % collisionless particles (type=2)
SofteningBulge  0.500    % collisionless particles (type=3)
SofteningStars  0.001    % stars spawned from gas (type=4)
SofteningBndry  0.001    % black holes (if active), or collisionless (type=5)
%---- if these are set in cosmo runs, SofteningX switches from comoving to physical
%------- units when the comoving value exceeds the choice here
%------- (these are ignored, and *only* the above are used, for non-cosmo runs)
SofteningGasMaxPhys     0.0005    % switch to 0.5pc physical below z=1 
SofteningHaloMaxPhys    0.010 
SofteningDiskMaxPhys    0.075 
SofteningBulgeMaxPhys   0.250 
SofteningStarsMaxPhys   0.0005 
SofteningBndryMaxPhys   0.0005 

