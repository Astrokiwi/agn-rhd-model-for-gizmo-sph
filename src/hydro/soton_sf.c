#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include "../allvars.h"
#include "../proto.h"

/*! \file soton_sf.c
 *  \brief  adds SF heating to gas particles
 */
/*
 * This file has been added to GIZMO by David John Williamson
 * please ask for permission to use
 * d.j.williamson@soton.ac.uk
 * david-john.williamson.1@ulaval.ca
 * but search for an updated email address
 */

/*! This is currently a placeholder to inject SN energy in cells
 */
void do_sf() {
    int i;
    //double SNe_rate = (.014 * 1.e6)*All.UnitTime_in_Megayears;
    //double SNe_rate = (.005 * 1.e6)*All.UnitTime_in_Megayears;
    //double SN_energy = 1.e51/All.UnitEnergy_in_cgs; // 1.e52 is fiducial, but lower for testing

    // xx loop through all active particles that exist
    // xx only active particles are updated 
    
    // loop through *all* particles!
    for(i = 0; i < NumPart; i++) {
    
//        if( P[i].Type==0 && P[i].Mass > 0 ) {// && TimeBinActive[P[i].TimeBin] ) {
        if( P[i].Type==0 && P[i].Mass > 0 && TimeBinActive[P[i].TimeBin] ) {
//        if ( 0 ) {
            
            integertime localdt_int = (P[i].TimeBin ? (((integertime) 1) << P[i].TimeBin) : 0);
            if ( localdt_int>0 ) {
                double localdt = All.Timebase_interval / All.cf_hubble_a * localdt_int;
                //double localdt = All.TimeStep;
                double sf_odds = All.imposed_SNR*localdt/All.TotN_gas;
                double ran = get_random_number(P[i].ID);
                //printf("SUPERNOVA %d %d %d %g %d %g %g %g\n",ThisTask,P[i].ID,All.N_SN_TOT,localdt,All.TotN_gas,SNe_rate,sf_odds,ran);
                //exit(1);
                if ( ran<=sf_odds ) {
                //if ( ran<=sf_odds && All.N_SN_TOT==0 ) {
                //if ( ran<=sf_odds && All.N_SN_TOT==0 && ThisTask==0) {
                //if ( All.N_SN_TOT==0 && ThisTask==13 ) {
                    SphP[i].dEnergySF = (All.SN_energy/(P[i].Mass));
                    All.N_SN_TOT++;
                    //printf("SUPERNOVA %d %d %g %d %g\n",ThisTask,All.N_SN_TOT,localdt,All.TotN_gas,sf_odds);
                } else {
                    SphP[i].dEnergySF = 0.0;
                }
            } else {
                SphP[i].dEnergySF = 0.0;
            }
        } else {
            SphP[i].dEnergySF = 0.0;
        }
    }
    //printf("ENERGY INPUT: %g\n",(1.e52/All.UnitEnergy_in_cgs/(P[0].Mass)));
}






