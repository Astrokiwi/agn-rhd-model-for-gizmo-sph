#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "../allvars.h"
#include "../proto.h"
#include <stdbool.h>

/*! \file soton_cool.c
 *  \brief  calculates the cooling rate for a particle
 */
/*
 * This file has been added to GIZMO by David John Williamson
 * please ask for permission to use
 * d.j.williamson@soton.ac.uk
 * david-john.williamson.1@ulaval.ca
 * but search for an updated email address
 */

#define COOLTAB_N 100
double coolInvLambdaIntTab[COOLTAB_N]; // integrated table for more accurate cooling
double logUMin,logUstep;

/*! Read in the cooling table. Currently just a simple placeholder
    Number of elements in array is hardcoded, assume input is evenly
    in logT
 */
void read_cool(void) {
    FILE *fd;
    int ic;
    double u_in, tab_in;
    // ** TODO - specify cooling table as a parameter
    if ( fd = fopen("./cooltab_int.dat", "r") ) {
        for ( ic=0 ; ic<COOLTAB_N ; ic++ ) {
            fscanf(fd,"%le %le",&u_in,&tab_in);
            coolInvLambdaIntTab[ic] = tab_in;
            if ( ic==0 ) logUMin = log(u_in);
            if ( ic==1 ) logUstep = log(u_in)-logUMin;
        } 
        fclose(fd);
    } else {
        if ( ThisTask==0 ) {
            printf("Cooling table not found\n");
        }
        endrun(1);
    }
}

double calc_DE_cool(int i, double dt) {
    const double molecular_mass = 4.0 / (1 + 3 * HYDROGEN_MASSFRAC);	/* note: assuming NEUTRAL GAS */
    const double K_TO_U = All.UnitMass_in_g / All.UnitEnergy_in_cgs*BOLTZMANN/GAMMA_MINUS1/(molecular_mass*PROTONMASS);
    const double cooling_ufloor = 15.*K_TO_U;// from K // dirty hack, should be a parameter
    
    double rho; // density in cgs
    double nH; // estimate of nH for particle

    double interpf;
    double logU;
    int itab;
    
    double tab0;
    bool interp_done;
    double uOut;
    
    
    //dt = 1e-7;
    //SphP[i].InternalEnergy*=1.e6;
    
    if ( dt<0. ) {
        // don't cool for correction step
        // probably should keep track of last updated time so we don't double-up
        // currently this means we do a half-step (or more) of extra cooling when we change the time-step
        SphP[i].DtEnergyCool=0;
    }
    
    if ( SphP[i].InternalEnergy>cooling_ufloor ) {
        rho = SphP[i].Density*All.cf_a3inv * All.UnitDensity_in_cgs * All.HubbleParam * All.HubbleParam; // density in cgs
        nH = rho/(molecular_mass*PROTONMASS);
        
        logU = log(SphP[i].InternalEnergy);
        itab = (int)floor((logU-logUMin)/logUstep);
        
        
        if ( itab<0 ) {
            return; // cooling below floor
            //tab0=coolInvLambdaIntTab[0];
        } else if ( itab>=COOLTAB_N-1 ) {
            // hit ceiling - force to be under 10^10 K
            tab0=coolInvLambdaIntTab[COOLTAB_N-1];
        } else {
            interpf = (logU - (itab*logUstep+logUMin))/logUstep;
            /*if ( interpf<0. ) {
                printf("interpf<0 :(\n");
                exit(0);
            }*/
            tab0 = (1.-interpf) * coolInvLambdaIntTab[itab] + interpf * coolInvLambdaIntTab[itab+1];
        }
        
//          if ( ThisTask==0 ) {
//              printf("%d %g %g %g %g %g\n",itab,tab0,nH * dt / molecular_mass,nH,dt,interpf);
//          }

        
        tab0 += nH * dt / molecular_mass; // protonmass is already included in table
        
        // interpolate inversely to get final internal energy
        
        // find last value >tab0
        // integrate becomes increasingly *negative*
        itab = 0;
        interp_done = false;
        while ( itab<COOLTAB_N && !interp_done ) {
            if ( coolInvLambdaIntTab[itab]>tab0 ) {
                itab++;
            } else {
                itab--;
                interp_done = true;
            }
        }

        if ( itab>=COOLTAB_N-1 ) {
            uOut=exp(logUMin+(COOLTAB_N-1)*logUstep);
        } else if (itab==-1 ) {
            uOut = exp(logUMin);
        } else {
            interpf = (coolInvLambdaIntTab[itab]-tab0)/(coolInvLambdaIntTab[itab]-coolInvLambdaIntTab[itab+1]);
             if ( ThisTask==0 ) {
                 //printf("%g %g %g %d %g\n",coolInvLambdaIntTab[itab],coolInvLambdaIntTab[itab+1],tab0,itab,interpf);
             }
            uOut = logUMin+(itab+interpf)*logUstep;
            uOut = exp(uOut);
        }
        
         if ( ThisTask==0 ) {
             //printf("%g %g\n",SphP[i].InternalEnergy/K_TO_U,uOut/K_TO_U);
         }
         //exit(0);
         /*if ( uOut>SphP[i].InternalEnergy ) {
            printf("HEATING IN COOLING!??\n");
            printf("Tin %g, Tout %g, uOut: %g, interpf: %g, itab: %d, tab0+nH*dt/mass: %g, tab0: %g, dt: %g, nH: %g\n",SphP[i].InternalEnergy/K_TO_U,uOut/K_TO_U,uOut,interpf,itab,tab0,tab0-nH * dt / molecular_mass,dt,nH);
            exit(0);
         }*/
         /*if ( SphP[i].InternalEnergy/K_TO_U>1.e8 ) {
            printf("%d %g %g %g %g\n",ThisTask,SphP[i].InternalEnergy/K_TO_U,uOut/K_TO_U,(SphP[i].InternalEnergy-uOut)/dt,dt*SphP[i].InternalEnergy/(SphP[i].InternalEnergy-uOut)*All.UnitTime_in_Megayears);
            exit(0);
         }*/
//         printf("%g %g\n",SphP[i].InternalEnergy,uOut);
//         exit(0);
        //SphP[i].InternalEnergy = uOut;
        SphP[i].DtEnergyCool=(uOut-SphP[i].InternalEnergy)/dt;
    } else {
        SphP[i].DtEnergyCool=0;
    }
}


/*! This is currently a placeholder to calculate the cooling rate for
 *  one particle/cell. Later this will be modified to include various
 *  chemistry terms etc.
 *
 *  This adds the cooling directly to the particle's internal energy
 */
void cool_kick_p(int i, double dt) {
    calc_DE_cool(i,dt);
    SphP[i].InternalEnergy+=SphP[i].DtEnergyCool*dt;
}

