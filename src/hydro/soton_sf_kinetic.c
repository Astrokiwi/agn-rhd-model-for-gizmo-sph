#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "../allvars.h"
#include "../proto.h"

static inline double square(double x) {
    return x*x;
}

#ifdef SOTON_SN_MASS 
typedef struct {
    int task;
    int loc;
    double dist2;
} dist_wrapper;

// sorts from *closest* to *furthest*
int compare_wrapped_dists(const void *a,const void *b) {
    dist_wrapper *x = (dist_wrapper *) a;
    dist_wrapper *y = (dist_wrapper *) b;
    if (x->dist2>y->dist2) return 1;
    if (x->dist2<y->dist2) return -1;
    return 0;
}
#endif

#ifdef SOTON_KINETIC_SF

void generate_SN() {
    // randomly calculate SN bubble position(s)
    // communicate to other nodes
    // add up number of particles within each bubble
    // communicate back to all other nodes
    // apply kinetic energy kicks to all particles in bubbles, even inactive ones

    double local_SN_odds;
    int local_SN;
    int global_SN[NTask];
    
    int SN_offsets[NTask];
    int N_SN;
    double *global_SN_coords;
    double *local_SN_coords;
    int *SN_N_part;
    
    int i, j, k;
    
    double ran;
    double rad,theta;
    double dist,dist2;
    
    double dK_SN;
    double v_dot_r_hat, dv;

    
    if ( All.imposed_SNR<=0 ) {
        return;
    }
    
#ifdef SOTON_FORCE_SN_RATE
    local_SN=0;
    if ( ThisTask==0 ) {
        // all supernovae generated on head
        local_SN_odds = All.imposed_SNR*All.Time - All.N_SN_TOT;
        if ( local_SN_odds>1. ) {
            local_SN=local_SN_odds; // truncated
        }
//         local_SN=1; // FOR TEST
    }
//     if ( ThisTask==0 ) {
//         // one SN, for test
//         if ( All.N_SN_TOT==0 && All.imposed_SNR*All.Time>1. ) {
//             local_SN=1;
//         }
//     }


#else
    local_SN_odds = All.imposed_SNR*All.TimeStep/NTask;
    local_SN = floor(local_SN_odds);
    local_SN_odds-= local_SN;
   
    if ( local_SN_odds>0 ) {
        ran = get_random_number(0); // argument is irrelevant if table is not precomputed
        
        if ( ran<local_SN_odds ) {
            local_SN++;
        }
    }
#endif

    All.N_SN_TOT+=local_SN;// this is *LOCAL* value
    
    MPI_Allgather(&local_SN, 1, MPI_INT, global_SN, 1, MPI_INT, MPI_COMM_WORLD);
    
    SN_offsets[0] = 0;
    for ( i=1 ; i<NTask ; i++ ) {
        SN_offsets[i] = SN_offsets[i-1]+global_SN[i-1];
    }

    N_SN = 0;
    for ( i=0 ; i<NTask ; i++ ) {
        N_SN+= global_SN[i];
    }
    
    if ( N_SN==0 ) { 
        return;
    }
    
    local_SN_coords = (double*) malloc(sizeof(double)*3*local_SN);
    global_SN_coords = (double*) malloc(sizeof(double)*3*N_SN);
    
    for ( i=0 ; i<local_SN ; i++ ) {
        ran = get_random_number(0);
        rad = sqrt(ran); // for equally spaced in 2D
        rad *= All.SN_scalerad;
        
        ran = get_random_number(0);
        theta = 2.0*M_PI*ran;
        
        local_SN_coords[i*3+0] = rad * cos(theta);
        local_SN_coords[i*3+1] = rad * sin(theta);
        local_SN_coords[i*3+2] = 0.;
        
        printf("NEW SUPERNOVA: %g %g %g %g %g\n",rad,theta,local_SN_coords[i*3+0],local_SN_coords[i*3+1],local_SN_coords[i*3+2]);
    }

     // for MPI packing
    for ( i=0 ; i<NTask ; i++ ) {
        SN_offsets[i]*=3;
        global_SN[i]*=3;
    }
    local_SN*=3;

    MPI_Allgatherv(local_SN_coords, local_SN, MPI_DOUBLE, global_SN_coords, global_SN, SN_offsets, MPI_DOUBLE, MPI_COMM_WORLD);

    SN_N_part = (int*) malloc(sizeof(int)*N_SN);
    for ( i=0 ; i<N_SN ; i++ ) {
        SN_N_part[i] = 0;
    }
    
#ifdef SOTON_SN_MASS 
    int SN_np_desired; // hard-coded for now
    dist_wrapper *local_dists = (dist_wrapper*) malloc(sizeof(dist_wrapper)*NumPart);
    dist_wrapper *global_dists;
    int np_proc[NTask],displ[NTask],nbytes_procs[NTask],np_hit[NTask];
    int iTask;
    int supernova_current_N;
    
        
    if ( ThisTask==0 ) {
        global_dists = (dist_wrapper*) malloc(sizeof(dist_wrapper)*All.TotNumPart);
//         printf("SN wanted: %d %g %g\n",All.supernova_target_N,All.SN_mass,P[0].Mass);
    }
    
    for ( i=0; i<NumPart; i++) {
        // here (and below) we could insert a check to see if T/v is too high to insert another SN?
        local_dists[i].task = ThisTask;
        local_dists[i].loc = i;
    }
    
    for ( j=0 ; j<N_SN ; j++ ) {
        for ( i=0; i<NumPart; i++) {
            local_dists[i].dist2 = 0;
            for ( k=0 ; k<3 ; k++ ) {
                local_dists[i].dist2+= pow(global_SN_coords[j*3+k]-P[i].Pos[k],2);
            }
        }

//         if ( ThisTask==0 ) {
//             for ( i=0; i<20; i++) {
//                 printf("%d %d %d %g\n",local_dists[i].task,i,local_dists[i].loc,local_dists[i].dist2);
//             }
//         }

        qsort(local_dists,NumPart,sizeof(dist_wrapper),compare_wrapped_dists);
        MPI_Gather(&NumPart,1,MPI_INT,np_proc,1,MPI_INT,0,MPI_COMM_WORLD);
        if ( ThisTask==0 ) {
            for ( iTask=0 ; iTask<NTask ; iTask++ ) {
                nbytes_procs[iTask] = np_proc[iTask]*sizeof(dist_wrapper);
            }
            displ[0] = 0;
            for ( iTask=1 ; iTask<NTask ; iTask++ ) {
                displ[iTask] = displ[iTask-1]+nbytes_procs[iTask-1];
            }
        }
        MPI_Gatherv(local_dists,NumPart*sizeof(dist_wrapper),MPI_BYTE,global_dists,nbytes_procs,displ,MPI_BYTE,0,MPI_COMM_WORLD);
        
        // if ( ThisTask==0 ) {
//             for ( i=0; i<20; i++) {
//                 printf("%d %d %d %g\n",global_dists[i].task,i,global_dists[i].loc,local_dists[i].dist2);
//             }
//             for ( i=50000; i<50020; i++) {
//                 printf("%d %d %d %g\n",global_dists[i].task,i,global_dists[i].loc,local_dists[i].dist2);
//             }
//         }
//         
        if ( ThisTask==0 ) {
            // sort closest across all procs
            for ( iTask=0 ; iTask<NTask ; iTask++ ) {
                displ[iTask]/=sizeof(dist_wrapper);
                np_hit[iTask]=0;
            }
            supernova_current_N = 0;
            while ( supernova_current_N<All.supernova_target_N) {
                int min_task=-1;
                double min_dist2;
                for ( iTask=0 ; iTask<NTask ; iTask++ ) {
                    if ( np_hit[iTask]<np_proc[iTask] ) {
                        if ( min_task==-1 ) {
                            min_task = iTask;
                            min_dist2 = global_dists[np_hit[iTask]+displ[iTask]].dist2;
                        } else if ( min_dist2>global_dists[np_hit[iTask]+displ[iTask]].dist2 ) {
                            min_task = iTask;
                            min_dist2 = global_dists[np_hit[iTask]+displ[iTask]].dist2;
                        }
                    }
                }
                
                if ( min_task==-1 ) {
                    printf("need more supernova particles than could possibly exist! ERROR\n");
                    exit(0);
                }
                
                np_hit[min_task]++;
                supernova_current_N++;
                
            }
//             for ( iTask=0 ; iTask<NTask ; iTask++ ) {
//                 printf("XX%d %d\n",iTask,np_hit[iTask]);
//             }
        }
        
        MPI_Scatter(np_hit,1,MPI_INT,&(SN_N_part[j]),1,MPI_INT,0,MPI_COMM_WORLD);
//         if ( SN_N_part[j]>0 ) {
//             printf("YY%d %d %g\n",ThisTask,SN_N_part[j],local_dists[SN_N_part[j]-1].dist2);
//         }        
//         MPI_Barrier(MPI_COMM_WORLD);
//         exit(0);
        for ( i=0 ; i<SN_N_part[j] ; i++ ) {
            apply_SN_kick(local_dists[i].loc,All.supernova_target_N,&(global_SN_coords[j*3]),local_dists[i].dist2);
//             apply_SN_thermal(i,SN_N_part[j]);
        }
    }
    
    free(local_dists);
    if ( ThisTask==0 ) {
        free(global_dists);
    }
#else   
    
    for( i=0; i<NumPart; i++) {
        for ( j=0 ; j<N_SN ; j++ ) {
            dist2 = 0;
            for ( k=0 ; k<3 ; k++ ) {
                dist2+= pow(global_SN_coords[j*3+k]-P[i].Pos[k],2);
            }
            if ( dist2<=pow(All.SN_rad,2) ) {
                SN_N_part[j]++;
            }
        }
    }
    
    MPI_Allreduce(MPI_IN_PLACE,SN_N_part,N_SN,MPI_INT,MPI_SUM,MPI_COMM_WORLD);

    if ( ThisTask==0 ) {
        printf("%d SN particles: %d\n",ThisTask,SN_N_part[0]);
    }

    for( i=0; i<NumPart; i++) {
        for ( j=0 ; j<N_SN ; j++ ) {
            dist2 = 0.;
            for ( k=0 ; k<3 ; k++ ) {
                dist2+= pow(global_SN_coords[j*3+k]-P[i].Pos[k],2);
            }
            if ( dist2<=pow(All.SN_rad,2) ) {
                apply_SN_kick(i,SN_N_part[j],&(global_SN_coords[j*3]),dist2);
//                 apply_SN_thermal(i,SN_N_part[j]);
            }
        }
    }
#endif
    
    free(local_SN_coords);
    free(global_SN_coords);
    free(SN_N_part);
//     MPI_Barrier(MPI_COMM_WORLD);
//     exit(0);
//     if ( ThisTask==0 ) {
//         printf("%d SUPERNOVA DONE\n");
//     }
    printf("%d SUPERNOVA DONE\n",ThisTask);
    MPI_Barrier(MPI_COMM_WORLD);

    if ( ThisTask==0 ) {
        printf("ALL SUPERNOVA DONE\n");
    }

}// 
// 
// void apply_SN_thermal(int i,int SN_p) {
// 
//     double dE_SN = All.SN_thermal_frac*All.SN_energy/SN_p;
//     SphP[i].dEnergySF += (dE_SN/(P[i].Mass));
//     
//     // Make the particle active so that dt gets updated?
//     if ( !TimeBinActive[P[i].TimeBin] ) {
//         NextActiveParticle[i] = FirstActiveParticle;
//         FirstActiveParticle = i;
//     }
// }

void apply_SN_kick(int i,int SN_p,double *SN_coords, double dist2) {
    double dv;
    int k;

    double dK_SN = (1.-All.SN_thermal_frac)*All.SN_energy/SN_p;
    double dE_SN = All.SN_thermal_frac*All.SN_energy/SN_p;
    double dist = sqrt(dist2);
    double v_dot_r_hat = 0.;

    v_dot_r_hat = 0.;

    for ( k=0 ; k<3 ; k++ ) {
        v_dot_r_hat+= (P[i].Pos[k]-SN_coords[k])*P[i].Vel[k];
    }
    v_dot_r_hat/=dist;

    dv = -v_dot_r_hat+sqrt(square(v_dot_r_hat)+2*dK_SN/P[i].Mass);

    for ( k=0 ; k<3 ; k++ ) {
        SphP[i].dMomentumSF[k] = dv*(P[i].Pos[k]-SN_coords[k])/dist * P[i].Mass;
//         SphP[i].dMomentumSF[k] = sqrt(dK_SN*P[i].Mass);
    }
    
    SphP[i].dEnergySF += (dE_SN/(P[i].Mass));

    // Make the particle active so that dt gets updated?
    if ( !TimeBinActive[P[i].TimeBin] ) {
        NextActiveParticle[i] = FirstActiveParticle;
        FirstActiveParticle = i;
    }
}
#endif

// star formation - for particles whose jeans' mass is less than (SOMEFACTOR) times the particle mass, convert to stars at some time-scale
// this will end up being very fast
//
// n.b. this will never trigger if the TrueLove heating floor is switched on!
#ifdef SFSINK
void sf_sink() {
    int i;
    double die_chance, die_timescale;
    double sink_dt;
    double ran;
    
    //double NJeans = 1;
    double mJeans2;
    
    //double h_eff,xJeans;
    
    double sf_mass_thisproc_thisit = 0.;
    double sf_mass_allprocs_thisit = 0.;
    
//     double rad2d2;
//     double sf_cutoff2 = pow(.7*All.inflowRad,2);
    
    const double JEANS_PREFACTOR = pow(M_PI,5)/144.*pow(GAMMA,3);
    const double FREEFALL_PREFACTOR = sqrt(3.*M_PI/32.);
    
    sink_dt = All.Time-All.lastSFSink;

    for ( i=0 ; i<NumPart ; i++ ) {
        if ( SphP[i].Density>All.thresholdSFDensity ) {

//             rad2d2 = pow(P[i].Pos[0],2)+pow(P[i].Pos[1],2);
            
            
//             if ( rad2d2<sf_cutoff2 ) {

                /*mJeans2 = JEANS_PREFACTOR*pow(SphP[i].Pressure/All.G/SphP[i].EgyWtDensity,3)/SphP[i].EgyWtDensity;
                printf("%g %g\n",sqrt(mJeans2),P[i].Mass);
                exit(0);
        
                if(mJeans2<=pow(P[i].Mass,2)) {*/
                    // 
                    die_timescale = FREEFALL_PREFACTOR/sqrt(All.G*SphP[i].Density);
                    die_chance = 1.-exp(-sink_dt/die_timescale);
                    ran = get_random_number(0); // argument is irrelevant if table is not precomputed
                    //printf("%g %g %g %g\n",sink_dt,die_timescale,die_chance,ran);
                    //exit(0);
                    if ( ran<die_chance ) {
                        sf_mass_thisproc_thisit+=P[i].Mass;
                        delete_particle(i);
                        //teleport_to_inflow(i);
                    }
                //}
//             }
        }
    }

    All.lastSFSink = All.Time;
    MPI_Allreduce(&sf_mass_thisproc_thisit, &sf_mass_allprocs_thisit, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    All.cumulativeStarFormationMass+=sf_mass_allprocs_thisit;

    
}
#endif
























