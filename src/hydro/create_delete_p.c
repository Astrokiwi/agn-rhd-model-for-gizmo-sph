#include <mpi.h>
#include <stdlib.h>
#include "../allvars.h"
#include "../proto.h"
#include <stdio.h>


#ifdef SOTON_AGN

/* "delete" a particle by teleporting it to the infall radius, so that infall == star formation + accretion */
void teleport_to_inflow(int i) {
    double ran,theta,rad,rad0;
    
    double vcirc;

    /* uniform distribution in ring */
    ran = get_random_number(0);
    theta = 2.0*M_PI*ran;
    
    /* random radius too */
//     ran = get_random_number(0);
//     rad = All.inflowRad+All.inflowRad/(ran+1.e-3);
    rad = All.inflowRad;
    
    rad0 = sqrt(P[i].Pos[0]*P[i].Pos[0]+P[i].Pos[1]*P[i].Pos[1]);

    /* conserve angular momentum */
    vcirc = (P[i].Pos[0]*P[i].Vel[1]-P[i].Pos[1]*P[i].Vel[0])/rad0;
    
    P[i].Pos[0] = rad * cos(theta);
    P[i].Pos[1] = rad * sin(theta);
    P[i].Pos[2] = 0.;

    P[i].Vel[0] = -P[i].Pos[1]*vcirc/rad;
    P[i].Vel[1] = P[i].Pos[0]*vcirc/rad;
    P[i].Vel[2] = 0.;
    
    /* boost the condition number to be conservative, so we don't trigger madness in the kernel */
    SphP[i].ConditionNumber *= 10.0;
}

/* for negative mass version, must be called before rearrange_particle_sequence in domain.c to delete particles immediately.
For teleport version, this can be called at any time */

void delete_particle(int i) {
#ifdef STEADY_STATE
    teleport_to_inflow(i);
#else
    P[i].Mass = 0.;
#endif
}


void cross_product(double *a,double *b,double *result) {
    result[0] = a[1]*b[2] - a[2]*b[1];
    result[1] = a[2]*b[0] - a[0]*b[2];
    result[2] = a[0]*b[1] - a[1]*b[0];
}

void create_particle_from(int i,int j) {
    if(NumPart + 1 >= All.MaxPart)
    {
        printf ("On Task=%d with NumPart=%d we try to create a particle. Sorry, no space left...(All.MaxPart=%d)\n", ThisTask, NumPart, All.MaxPart);
        fflush(stdout);
        endrun(8888);
    }
    if(P[i].Type != 0) {printf("SPLITTING NON-GAS-PARTICLE: i=%d ID=%d Type=%d \n",i,P[i].ID,P[i].Type); fflush(stdout); endrun(8889);}
    
    /* find the first non-gas particle and move it to the end of the particle list */
    //long j = NumPart;
    /* set the pointers equal to one another -- all quantities get copied, we only have to modify what needs changing */
    P[j] = P[i]; SphP[j] = SphP[i];

    /* the particle needs to be 'born active' and added to the active set */
    NextActiveParticle[j] = FirstActiveParticle;
    FirstActiveParticle = j;
    NumForceUpdate++;
    /* likewise add it to the counters that register how many particles are in each timebin */
    TimeBinCount[P[j].TimeBin]++;
    PrevInTimeBin[j] = i;
    NextInTimeBin[j] = NextInTimeBin[i];
    if(NextInTimeBin[i] >= 0) {PrevInTimeBin[NextInTimeBin[i]] = j;}
    NextInTimeBin[i] = j;
    if(LastInTimeBin[P[i].TimeBin] == i) {LastInTimeBin[P[i].TimeBin] = j;}
    
    /* boost the condition number to be conservative, so we don't trigger madness in the kernel */
    SphP[j].ConditionNumber *= 10.0;

}

/* based on split_particle_i from existing code. Copy particle i, but give new properties to it*/
void create_inflow_ring_particle_from(int i, int j)
{
    double ran,theta;
    
    create_particle_from(i,j);

    /* uniform distribution across surface of sphere */
    ran = get_random_number(0);
    theta = 2.0*M_PI*ran;
    ran = get_random_number(0)*2.-1.;
    
    P[j].Pos[0] = All.inflowRad * cos(theta)*sqrt(1-ran*ran);
    P[j].Pos[1] = All.inflowRad * sin(theta)*sqrt(1-ran*ran);
    P[j].Pos[2] = All.inflowRad * ran;

//     P[j].Pos[0] = 1.e-3;
//     P[j].Pos[1] = 1.e-3;
//     P[j].Pos[2] = 1.e-3;

    P[j].Vel[0] = 0.;
    P[j].Vel[1] = 0.;
    P[j].Vel[2] = 0.;
    
    // TODO - set internal energy/temperature of this gas!

    // need to assign new particle a unique ID:
    P[j].ID = newPID;
    newPID+=NTask;

    SphP[j].Pressure = get_pressure(j);
        
    Gas_split++;

    /* only calling the create/delete algorithm when we're doing the new domain decomposition */
}

#ifdef GEN_WIND
/* based on split_particle_i from existing code. Copy particle i, but give new properties to it*/
void create_wind_particle_from(int i, int j)
{
    double cosphi,sinphi,theta;
    
    int kk;
    
    double rad2d;
    
    create_particle_from(i,j);

    /* uniform distribution across surface of sphere */
    cosphi = get_random_number(0)*All.outflowCosThetaWidth+All.outflowCosTheta0;
    sinphi=sqrt(1.-cosphi*cosphi);
    theta = 2.0*M_PI*get_random_number(0);
    
    rad2d = All.outflowRad*sinphi;
    
    P[j].Pos[0] = rad2d * cos(theta);
    P[j].Pos[1] = rad2d * sin(theta);
    
    P[j].Pos[2] = All.outflowRad * cosphi;
    if ( get_random_number(0)<0.5 ) {
        P[j].Pos[2]*=-1.;
    }

    // radial velocity
    for ( kk=0 ; kk<3 ; kk++ ) {
        P[j].Vel[kk] = P[j].Pos[kk]*All.outflowVel/All.outflowRad;
    }
    
    double vcirc = All.outflowCircVel*sqrt(All.outflowRad*All.outflowSourceRad)/rad2d;
    
    // circular velocity
    P[j].Vel[0] += P[j].Pos[1]*vcirc/rad2d;
    P[j].Vel[1] -= P[j].Pos[0]*vcirc/rad2d;
    
    // TODO - set internal energy/temperature of this gas?

    // need to assign new particle a unique ID:
    P[j].ID = newPID;
    newPID+=NTask;

    SphP[j].Pressure = get_pressure(j);
        
    Gas_split++;

    /* only calling the create/delete algorithm when we're doing the new domain decomposition */
}
#endif

/* create the correct number of new particles */
void create_inflow() {
    double dt_inflow;
    int np_gen;
    int j;
    
    dt_inflow = All.Time-All.lastInflowGeneration;
    All.lastInflowGeneration = All.Time;
    
    All.inflow_massgen += dt_inflow * All.inflowRate/NTask;
    
    np_gen = All.inflow_massgen/P[0].Mass;
    
    All.inflow_massgen -= np_gen * P[0].Mass;

    for ( j=0 ; j<np_gen ; j++ ) {
        create_inflow_ring_particle_from(0,NumPart+Gas_split);
    }

// All processes process the same number number of particles each
    All.TotNumPart += np_gen*NTask;
    All.TotN_gas += np_gen*NTask;


//     MPI_Barrier(MPI_COMM_WORLD);
//     MPI_Finalize();
//     exit(0);
}


#ifdef GEN_WIND
/* create the correct number of new particles */
void create_outflow() {
    double dt_outflow;
    int np_gen;
    int j;
    
    dt_outflow = All.Time-All.lastOutflowGeneration;
    All.lastOutflowGeneration = All.Time;
    
    All.outflow_massgen += dt_outflow * All.outflowRate/NTask;
    
    np_gen = All.outflow_massgen/P[0].Mass;
    
//     printf("%d particle generation %g\n",ThisTask,All.outflow_massgen/P[0].Mass);
    
    All.outflow_massgen -= np_gen * P[0].Mass;

    for ( j=0 ; j<np_gen ; j++ ) {
        create_wind_particle_from(0,NumPart+Gas_split);
    }

// All processes process the same number number of particles each
    All.TotNumPart += np_gen*NTask;
    All.TotN_gas += np_gen*NTask;


//     MPI_Barrier(MPI_COMM_WORLD);
//     MPI_Finalize();
//     exit(0);
}
#endif

#ifdef DELETE_OUTFLOW
void delete_distant_particles() {
    int i,j;
    double rad;

    for (i=0 ; i<NumPart ; i++ ) {
        rad = 0;
        for ( j=0 ; j<3 ; j++ ) {
            rad+=(P[i].Pos[j])*(P[i].Pos[j]);
        }
        if ( rad>All.outerDeleteRad2 )  {
            delete_particle(i);
        }
    }
}
#endif

double kill_if_touching_bh_getmomentum(double *kill_point, double *momentum_deleted_thiscall, double *j_deleted_thiscall) {
    double mass_deleted_thiscall = 0.;

    double rad;
    
    int i,j;
    
    double j_mom[3],dr[3];;

    for ( j=0 ; j<3 ; j++ ){
        momentum_deleted_thiscall[j] = 0.;
        j_deleted_thiscall[j] = 0.;
    }
    
    for (i=0 ; i<NumPart ; i++ ) {
        rad = 0;
        for ( j=0 ; j<3 ; j++ ) {
            dr[j] = P[i].Pos[j]-kill_point[j];
            rad+=dr[j]*dr[j];
        }
        rad = sqrt(rad);
        if ( rad<All.innerKillRad+P[i].Hsml )  {
//             printf("KILLED A PARTICLE %g %g %g\n",rad,All.innerKillRad,P[i].Hsml);
            mass_deleted_thiscall+=P[i].Mass;
//             printf("KILLED A PARTICLE %g %g %g %g %g %g %g %g\n",rad,All.innerKillRad,P[i].Hsml,P[i].Mass,mass_deleted_thiscall,P[i].Vel[0],P[i].Vel[1],P[i].Vel[2]);
//             exit(0);
            
            cross_product(P[i].Pos,P[i].Vel,j_mom);
            
            for ( j=0 ; j<3 ; j++ ) {
                momentum_deleted_thiscall[j] += P[i].Mass*P[i].Vel[j];
                j_deleted_thiscall[j] += P[i].Mass*j_mom[j];
            }
            
            
            delete_particle(i);
        }
    }
    return mass_deleted_thiscall;
}

double kill_if_touching_bh(double *kill_point) {
    double dummy[3];
    return kill_if_touching_bh_getmomentum(kill_point,dummy,dummy);
}

/* mark particles for death*/
void update_deathzone() {
    int i;
    

#ifdef BINARY_BH
    double momentum_deleted_thisproc_thisit_1[3];
    double momentum_deleted_thisproc_thisit_2[3];
    
    double momentum_deleted_allprocs_thisit_1[3];
    double momentum_deleted_allprocs_thisit_2[3];
    
    double J_deleted_thisproc_thisit_1[3];
    double J_deleted_thisproc_thisit_2[3];
    double J_deleted_allprocs_thisit_1[3];
    double J_deleted_allprocs_thisit_2[3];

    double mass_deleted_thisproc_thisit_1 = 0.;
    double mass_deleted_allprocs_thisit_1;
    double mass_deleted_thisproc_thisit_2 = 0.;
    double mass_deleted_allprocs_thisit_2;

    
    for ( i=0 ; i<3 ; i++ ) {
        momentum_deleted_thisproc_thisit_1[i] = 0.;
        momentum_deleted_thisproc_thisit_2[i] = 0.;
        momentum_deleted_allprocs_thisit_1[i] = 0.;
        momentum_deleted_allprocs_thisit_2[i] = 0.;

        J_deleted_thisproc_thisit_1[i] = 0.;
        J_deleted_thisproc_thisit_2[i] = 0.;
        J_deleted_allprocs_thisit_1[i] = 0.;
        J_deleted_allprocs_thisit_2[i] = 0.;
    }
    mass_deleted_thisproc_thisit_1=kill_if_touching_bh_getmomentum(All.BH_binary.Pos_1,momentum_deleted_thisproc_thisit_1,J_deleted_thisproc_thisit_1);
    mass_deleted_thisproc_thisit_2=kill_if_touching_bh_getmomentum(All.BH_binary.Pos_2,momentum_deleted_thisproc_thisit_2,J_deleted_thisproc_thisit_2);


    MPI_Allreduce(&mass_deleted_thisproc_thisit_1, &mass_deleted_allprocs_thisit_1, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    MPI_Allreduce(&mass_deleted_thisproc_thisit_2, &mass_deleted_allprocs_thisit_2, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

    All.BH_binary.accreted_mass_1+=mass_deleted_allprocs_thisit_1;
    All.BH_binary.accreted_mass_2+=mass_deleted_allprocs_thisit_2;
    
    All.cumulativeKillMass+=mass_deleted_allprocs_thisit_1+mass_deleted_allprocs_thisit_2;

    MPI_Allreduce(momentum_deleted_thisproc_thisit_1, momentum_deleted_allprocs_thisit_1, 3, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    MPI_Allreduce(momentum_deleted_thisproc_thisit_2, momentum_deleted_allprocs_thisit_2, 3, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

    MPI_Allreduce(J_deleted_thisproc_thisit_1, J_deleted_allprocs_thisit_1, 3, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    MPI_Allreduce(J_deleted_thisproc_thisit_2, J_deleted_allprocs_thisit_2, 3, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

    for ( i=0 ; i<3 ; i++ ) {
        All.BH_binary.accreted_momentum_1[i]+=momentum_deleted_allprocs_thisit_1[i];
        All.BH_binary.accreted_momentum_2[i]+=momentum_deleted_allprocs_thisit_2[i];

        All.BH_binary.accreted_J_1[i]+=J_deleted_allprocs_thisit_1[i];
        All.BH_binary.accreted_J_2[i]+=J_deleted_allprocs_thisit_2[i];
    }
#else
    double mass_deleted_thisproc_thisit = 0.;
    double mass_deleted_allprocs_thisit;

    double r_agn[3];
    for ( i=0 ; i<3 ; i++) r_agn[i]=0;
    mass_deleted_thisproc_thisit+=kill_if_touching_bh(r_agn);
   
    MPI_Allreduce(&mass_deleted_thisproc_thisit, &mass_deleted_allprocs_thisit, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    All.cumulativeKillMass+=mass_deleted_allprocs_thisit;
#endif 

#ifdef DELETE_OUTFLOW
    // Don't count that as accreted mass
    delete_distant_particles();
#endif


}

/* find the greatest ID of all particles, to choose what new particle IDs will be*/
void setup_new_IDs() {
    int localMaxID;
    int globalMaxID;
    int i;

    localMaxID=0;
    for ( i=0 ; i<NumPart ; i++ ) {
        if ( P[i].ID>localMaxID ) localMaxID=P[i].ID;
    }
    
    MPI_Allreduce(&localMaxID, &globalMaxID, 1, MPI_INT, MPI_MAX, MPI_COMM_WORLD);
    newPID = globalMaxID+ThisTask+1;
}

#endif