#define _USE_MATH_DEFINES
#include <math.h>

#include "kepler.h"
#include "../allvars.h"
#include <iostream>

StellarBinary::StellarBinary(double m1 // mass of one object
                ,double m2 // mass of other object
                ,double eccentricity_in // eccentricity of binary 
                ,double separation //  
                ) {

    double mtot = m1+m2;
    this->angular_freq = sqrt((All.G*mtot)/(separation*separation*separation));
    
    eccentricity = eccentricity_in;

    double minor_major_axis_factor = sqrt(1-eccentricity*eccentricity);
    
    semi_major_axis_1 = separation*m2/mtot;
    semi_minor_axis_1 = minor_major_axis_factor*semi_major_axis_1;
    semi_major_axis_2 = separation*m1/mtot;
    semi_minor_axis_2 = minor_major_axis_factor*semi_major_axis_2;
    
    
}

double StellarBinary::eccentric_anomaly(
                         double mean_anomaly,
                         bool verbose
                         ) {
    double cosE;
//     double guess = M_PI;
    double guess = mean_anomaly; // will sometimes fail to converge, for very elliptical orbits
    double oldguess;
    int n_its = 0;
    
    do {
        oldguess=guess;
        cosE = cos(oldguess);
        guess = (mean_anomaly - eccentricity * (oldguess*cosE - sin(oldguess)))
                /(1 - eccentricity*cosE);
        if ( ThisTask==0 ) {
            n_its++;
            if ( verbose ) {
                std::cout << "CHECKING CONVERGENCE:" << mean_anomaly << " " << n_its << " " << oldguess << " " << guess << std::endl;
            }
            if ( n_its>1000 ) {
                if ( verbose ) {
                    std::cout << "FAILURE TO CONVERGE " << guess << " " << oldguess << std::endl;
                    exit(0);
                } else {
                    eccentric_anomaly(mean_anomaly,true);
                }
            }
        }
    } while (fabs((oldguess-guess)/guess)>convergence_critera);
    
    
    return guess;
}

void StellarBinary::update(double t // time in years elapsed
                ) {
    double mean_anomaly = t*this->angular_freq;
    
    double theta = eccentric_anomaly(mean_anomaly);
    double costheta = cos(theta);
    double sintheta = sin(theta);
    double rad_factor = (1.-eccentricity*costheta);

    rad_1 = semi_major_axis_1*rad_factor;
    rad_2 = semi_major_axis_2*rad_factor;
    
    r_1[1] = semi_major_axis_1*costheta - eccentricity*semi_major_axis_1;
    r_1[0] = semi_minor_axis_1*sintheta;

    r_2[1] = -(semi_major_axis_2*costheta - eccentricity*semi_major_axis_2);
    r_2[0] = -(semi_minor_axis_2*sintheta);
    
}