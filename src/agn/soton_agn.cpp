#include <mpi.h>
#include <stdlib.h>
#include <string>
#include <cstring>
#include <memory>
#include "../allvars.h"
extern "C"
{
#include "../proto.h"
}
#include <stdio.h>

#include <iostream>
#include <algorithm>
#include "sph_raytrace/rt_prototypes.h"
#include "sph_raytrace/raytrace_helpers.h"
#include "sph_raytrace/raytracing.h"
#include "sph_raytrace/GizmoCoupler.h"
#include "kepler.h"

// #include <fenv.h>
// #include <csignal>



/*! \file soton_agn.c
 *  \brief  adds AGN heating and rad pressure to gas particles
 */
/*
 * This file has been added to GIZMO by David John Williamson
 * please ask for permission to use
 * d.j.williamson@soton.ac.uk
 * david-john.williamson.1@ulaval.ca
 * but search for an updated email address
 */
 
#ifdef ELLIPTICAL_BINARY
std::unique_ptr<StellarBinary> binary_tracker = nullptr;
#endif
 

//void agn_calc_heat_p_raytraced(int i, double coldens, double skin_rad);
//void agn_calc_heat_p_raytraced(int i, double tau);
void do_agn(bool applyHeat);
void do_binary_agn(bool applyHeat);
void setup_binary();
//static inline int agn_tab_index(int id, int it, int ii, int is);

// prototypes for *external* functions
extern "C"
{
    void do_agn();
    void do_binary_agn();
    void agn_heat_p(int i);
    void setup_agn();
    void agn_coolheat_steps(int i, double dt_full);
    void set_binary_location();
}

void check_speed_of_light() {
    int i,j;
    double v_2;
    
    for ( i=0 ; i<NumPart ; i++ ) {
        v_2 = 0.;
        for ( j=0 ; j<3 ; j++ ) v_2 += square(P[i].Vel[j]);
        if (v_2>1.e8 ) {
            // v in km/s
            //v^2<1e8 -> v>3c/100
            std::cout << "BIG SPEED FOUND " << v_2 << std::endl;
            exit(0);
        }

    }
}

#ifdef BINARY_BH
void setup_binary() {
    double total_bh_mass, v;

    int j;

    total_bh_mass = All.BH_binary.mass_1 + All.BH_binary.mass_2;
    
    All.BH_binary.Pos_1[0] =  All.BH_binary.distance * All.BH_binary.mass_2/total_bh_mass;
    All.BH_binary.Pos_2[0] = -All.BH_binary.distance * All.BH_binary.mass_1/total_bh_mass;
    All.BH_binary.Pos_1[1] = 0.0;
    All.BH_binary.Pos_2[1] = 0.0;
    All.BH_binary.Pos_1[2] = 0.0;
    All.BH_binary.Pos_2[2] = 0.0;

    // n.b. Vel is unused
    v = sqrt(((1-All.BH_binary.eccentricity)*All.G*total_bh_mass)/All.BH_binary.distance);
    All.BH_binary.Vel_1[0] = 0.0;
    All.BH_binary.Vel_2[0] = 0.0;
    All.BH_binary.Vel_1[1] =-v/(All.BH_binary.mass_1/All.BH_binary.mass_2+1.0);
    All.BH_binary.Vel_2[1] = v/(All.BH_binary.mass_2/All.BH_binary.mass_1+1.0);
    All.BH_binary.Vel_1[2] = 0.0;
    All.BH_binary.Vel_2[2] = 0.0;

    // used for torque
    All.BH_binary.F_old_1[0] = -All.G*All.BH_binary.mass_1*All.BH_binary.mass_2/(All.BH_binary.distance*All.BH_binary.distance);
    All.BH_binary.F_old_2[0] =  All.G*All.BH_binary.mass_1*All.BH_binary.mass_2/(All.BH_binary.distance*All.BH_binary.distance);
    All.BH_binary.F_old_1[1] = 0.0;
    All.BH_binary.F_old_2[1] = 0.0;
    All.BH_binary.F_old_1[2] = 0.0;
    All.BH_binary.F_old_2[2] = 0.0;


    All.BH_binary.accreted_mass_1 = 0.;
    All.BH_binary.accreted_mass_2 = 0.;

    for ( j=0 ; j<3 ; j++ ) {
        All.BH_binary.accreted_momentum_1[j] = 0.;
        All.BH_binary.accreted_momentum_2[j] = 0.;
    }

#ifdef ELLIPTICAL_BINARY
    binary_tracker = std::unique_ptr<StellarBinary>(new StellarBinary(
                                    All.BH_binary.mass_1
                                    ,All.BH_binary.mass_2
                                    ,All.BH_binary.eccentricity
                                    ,All.BH_binary.distance
                                    ));
#endif

    set_binary_location();
}

void set_binary_location() {
#ifdef ELLIPTICAL_BINARY
    // elliptical orbit, need to do proper newton-raphson etc
    binary_tracker->update(All.Time+All.binary_time_offset);

    All.BH_binary.Pos_1[0] = binary_tracker->r_1[0];
    All.BH_binary.Pos_2[0] = binary_tracker->r_2[0];

    All.BH_binary.Pos_1[1] = binary_tracker->r_1[1];
    All.BH_binary.Pos_2[1] = binary_tracker->r_2[1];
    
#else
    // circular orbit, just use sin/cos (t * angular speed)
    double bh_pos_angle,dist,total_bh_mass;
    total_bh_mass = All.BH_binary.mass_1 + All.BH_binary.mass_2;


    bh_pos_angle = sqrt(All.G*(total_bh_mass)/cube(All.BH_binary.distance))*(All.Time+All.binary_time_offset);
    dist = 0.0;
    dist = All.BH_binary.distance;

    All.BH_binary.Pos_1[0] =  dist * All.BH_binary.mass_2/(total_bh_mass) * cos(bh_pos_angle);
    All.BH_binary.Pos_2[0] = -dist * All.BH_binary.mass_1/(total_bh_mass) * cos(bh_pos_angle);
    All.BH_binary.Pos_1[1] =  dist * All.BH_binary.mass_2/(total_bh_mass) * sin(-bh_pos_angle);
    All.BH_binary.Pos_2[1] = -dist * All.BH_binary.mass_1/(total_bh_mass) * sin(-bh_pos_angle);
#endif

    // all in z=0 plane
    All.BH_binary.Pos_1[2] = 0.0;
    All.BH_binary.Pos_2[2] = 0.0;
}
#endif

/*! do all the initialisation required for the AGN
*/
void setup_agn() {

//    setup_raytracing();
    setup_new_IDs();
    
    setup_tables(  All.AGNTabLabelFile,All.AGNTabFile
                    ,All.AGNDustlessTabLabelFile,All.AGNDustlessTabFile
                    ,All.AGNHighdenseTabLabelFile,All.AGNHighdenseTabFile
                  );

//     setup_tables();
#ifdef BINARY_BH
    setup_binary();
#endif        
    
    if(RestartFlag==0) {
    
    // Set up initial values:
    // This is really just so we have a guess of the initial opacity
    // It's okay for the first guess of opacity to be a bit off, because that's a second order effect
    // but we don't want DtEnergyAGN or RadAccel to have weird values, because these will affect the simulation directly
        for ( int i=0 ; i<NumPart ; i++ ) {

#ifdef FIXED_TAU
            SphP[i].OpticalDepth = All.tau_fixed;
#else 
            SphP[i].OpticalDepth = 0.5; // For initial guess
#endif        

#ifdef BINARY_BH
            SphP[i].OpticalDepth_2 = SphP[i].OpticalDepth;
#endif        

            SphP[i].dusty = (SphP[i].InternalEnergy<=All.u_DustSput);

            SphP[i].DtEnergyAGN = 0.;

#ifdef SOTON_AGN_PRES
            for ( int jj=0 ; jj<3 ; jj++ ) {
                SphP[i].RadAccel[jj] = 0.;
            }
#endif

            agn_calc_heat_cool_p_raytraced_tab(i);
    //         agn_heat_p(i);

        }
    
        // I think we need to do a full raytracing step here, otherwise many things won't update until after a few ticks
        // do it twice to get ok opacities
        // "false" means don't apply energy
#ifdef BINARY_BH
      do_binary_agn(false);
      do_binary_agn(false);
#else
      do_agn(false);
      do_agn(false);
#endif
    }


#ifdef BINARY_BH
    set_binary_location();
#endif
}

void do_agn() {
#ifdef BINARY_BH
//     if(All.Time*0.9778e9>2e4){
      do_binary_agn(true);
//     }
#else
    do_agn(true);
#endif


//     check_speed_of_light();
}








/*! heat all active particles
    Note - this might be assuming that we only have gas particles!
    This routine might need to be modified if there are some star
    particles in the simulation.
*/
#ifdef BINARY_BH
void do_binary_agn(bool applyHeat) {
//     feenableexcept(FE_ALL_EXCEPT & ~FE_INEXACT & ~FE_UNDERFLOW); 



    // counters
    int i;
    int ic;
    
    double tstart,tend,tdifftree,tdiffdepth,tdiffagn;
    double t0,t1;

    t0 = my_second();

#ifndef FIXED_TAU
// build absorption tree
//     auto raytracer = std::make_unique<Raytracer>();
    std::shared_ptr<GizmoCoupler> gizmoCoupler(new GizmoCoupler());
    std::unique_ptr<Raytracer> raytracer(new Raytracer(gizmoCoupler));



    tstart = my_second();
    raytracer->build_tree();
    tend = my_second();
    tdifftree = timediff(tstart,tend);
    
    double **AGN_localtau = new double*[2];
    for(int i=0; i<2; i++){
      AGN_localtau[i] = new double[All.TotN_gas];
    }
    double r_agn[2][3];
#ifdef SOTON_DUST_DUST_HEATING    
    bool alreadyDone[2][All.TotN_gas];
#else
    bool alreadyDone[2][N_gas];
#endif

    // set AGN location (parameter later?)
    for ( ic=0 ; ic<3 ; ic++ ) {
        r_agn[0][ic] = All.BH_binary.Pos_1[ic];
        r_agn[1][ic] = All.BH_binary.Pos_2[ic];
    }
    
    tstart = my_second();
    raytracer->agn_optical_depths(r_agn[0],AGN_localtau[0],false);
    raytracer->agn_optical_depths(r_agn[1],AGN_localtau[1],false);
    tend = my_second();
    tdiffdepth = timediff(tstart,tend);
    

#endif    
    

    // calculate column density to each active local particle
    // store this, and calculate heating and cooling and rad pressure from this in call from kicks.c
    for(i = 0; i < NumPart; i++) {
#ifdef SOTON_AGN_DOUBLESTART
        if( P[i].Type==0 && P[i].Mass > 0 && ( TimeBinActive[P[i].TimeBin] || All.NumCurrentTiStep<=1)) {
#else
        if( P[i].Type==0 && P[i].Mass > 0 && TimeBinActive[P[i].TimeBin] ) {
#endif

#ifndef FIXED_TAU
            SphP[i].OpticalDepth = AGN_localtau[0][i]; // SET/GET OFFSETS CORRECTLY
            SphP[i].OpticalDepth_2 = AGN_localtau[1][i];
#endif

            agn_calc_heat_cool_p_raytraced_tab(i); // with allreduce, table
        }
    }

    // don't want to apply heat in initialisation - this doubles things up
    if ( applyHeat ) {
    // loop through active local particles, adding heating to each particle
        for(i = 0; i < NumPart; i++) {
#ifdef SOTON_AGN_DOUBLESTART
            if( P[i].Type==0 && P[i].Mass > 0 && ( TimeBinActive[P[i].TimeBin] || All.NumCurrentTiStep<=1)) {
#else
            if( P[i].Type==0 && P[i].Mass > 0 && TimeBinActive[P[i].TimeBin] ) {
#endif
                agn_heat_p(i);
            }
        }
    
    }

#ifdef SOTON_DUST_DUST_HEATING
    do_dust_dust_heating(alreadyDone, localOffset, localNActive, localIndex, nActiveTot,&tdiffwait,&tdifftree,&tdiffray);
#endif

    //std::cout << ThisTask << " " << timings[7] << " " << timings[8] << std::endl;
#ifndef FIXED_TAU
    delete[] AGN_localtau[0];
    delete[] AGN_localtau[1];
    delete[] AGN_localtau;
#endif
// 
    t1 = WallclockTime = my_second();
    tdiffagn = timediff(t0,t1);

    CPU_Step[CPU_SOTONAGNTREE] += tdifftree;
    CPU_Step[CPU_SOTONAGN] += tdiffagn-tdiffdepth-tdifftree;

}
#endif








/*! heat all active particles
    Note - this might be assuming that we only have gas particles!
    This routine might need to be modified if there are some star
    particles in the simulation.
*/
void do_agn(bool applyHeat) {
    // counters
    int i;
    int ic;
    

#ifndef FIXED_TAU
    double *AGN_localtau = new double[All.TotN_gas]; // could be made smaller and more efficient
//     auto raytracer = std::make_unique<Raytracer>();
    std::shared_ptr<GizmoCoupler> gizmoCoupler(new GizmoCoupler());
    std::unique_ptr<Raytracer> raytracer(new Raytracer(gizmoCoupler));
#endif
    double r_agn[3];
    
    double tstart,tend,tdifftree,tdiffdepth,tdiffagn;
    double t0,t1;

    t0 = my_second();

    // set AGN location (parameter later?)
    for ( ic=0 ; ic<3 ; ic++ ) {
        r_agn[ic] = 0.;
    }

#ifndef FIXED_TAU
// build absorption tree
    tstart = my_second();
    raytracer->build_tree();
    tend = my_second();
    tdifftree = timediff(tstart,tend);
    
    tstart = my_second();
    raytracer->agn_optical_depths(r_agn,AGN_localtau,true);
    tend = my_second();
    tdiffdepth = timediff(tstart,tend);
#endif

    for(i = 0; i < NumPart; i++) {
#ifdef SOTON_AGN_DOUBLESTART
        if( P[i].Type==0 && P[i].Mass > 0 && ( TimeBinActive[P[i].TimeBin] || All.NumCurrentTiStep<=1)) {
#else
        if( P[i].Type==0 && P[i].Mass > 0 && TimeBinActive[P[i].TimeBin] ) {
#endif
#ifndef FIXED_TAU
            SphP[i].OpticalDepth = AGN_localtau[i];
#endif
            agn_calc_heat_cool_p_raytraced_tab(i); // with allreduce, table
        }
    }
    
    // don't want to apply heat in initialisation - this doubles things up
    if ( applyHeat ) {
    // loop through active local particles, adding heating to each particle
        for(i = 0; i < NumPart; i++) {
#ifdef SOTON_AGN_DOUBLESTART
            if( P[i].Type==0 && P[i].Mass > 0 && ( TimeBinActive[P[i].TimeBin] || All.NumCurrentTiStep<=1)) {
#else
            if( P[i].Type==0 && P[i].Mass > 0 && TimeBinActive[P[i].TimeBin] ) {
#endif
                agn_heat_p(i);
            }
        }
    
    }

#ifdef SOTON_DUST_DUST_HEATING
    do_dust_dust_heating(alreadyDone, localOffset, localNActive, localIndex, nActiveTot,&tdiffwait,&tdifftree,&tdiffray);
#endif

    //std::cout << ThisTask << " " << timings[7] << " " << timings[8] << std::endl;
#ifndef FIXED_TAU
    delete[] AGN_localtau;
#endif

    t1 = WallclockTime = my_second();
    tdiffagn = timediff(t0,t1);

    CPU_Step[CPU_SOTONAGNTREE] += tdifftree;
    CPU_Step[CPU_SOTONAGN] += tdiffagn-tdiffdepth-tdifftree;
} 




void agn_heat_p(int i) {
   SphP[i].DtInternalEnergy+=SphP[i].DtEnergyAGN;

#ifdef SOTON_DUST_DUST_HEATING    
   SphP[i].DtInternalEnergy+=SphP[i].DtEnergyIR;
#endif
}

