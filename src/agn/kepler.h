/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   kepler.h
 * Author: davidjwilliamson
 *
 * Created on 15 April 2021, 09:39
 */

#ifndef KEPLER_H
#define KEPLER_H


class StellarBinary {
    private:

        double eccentric_anomaly(double mean_anomaly,
                                 bool verbose=false);

    public:
        StellarBinary(double m1 // mass of one star in Msun
                ,double m2 // mass of one star in Msun
                ,double eccentricity_in // eccentricity of binary 
                ,double separation // in pc 
                );
        
        void update(double t);
        
        double rad_1,rad_2;
        double r_1[2],r_2[2];
        
        double convergence_critera = 1.e-6;

        double semi_major_axis_1,semi_minor_axis_1,semi_major_axis_2,semi_minor_axis_2;
        double eccentricity;
        double angular_freq;


};



#endif /* KEPLER_H */

