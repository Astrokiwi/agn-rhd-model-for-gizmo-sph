#include <algorithm>
#include <iostream>
#include <vector>

#include "../allvars.h"
extern "C"
{
#include "../proto.h"
}
#include "cpp_prototypes.h"
#include "cpp_helpers.h"

/* Oct-tree of maximum luminosities for determining which particles
receive non-negligible heating. Here, particles are placed in unique
leaf nodes.
*/


// a good value for this depends on the particular simulation and is not at all a universal value!
// units of inverse mass (1/(1e10 Msun))
// static const double lum_cutoff = 2000*1.e12;
//static const double lum_cutoff = 1.e-4;//1.e-5; // default
static const double lum_cutoff = 1.e-5;//1.e-5;
static const double angle_cutoff_deg = 0.;// 5
static const double angle2_cutoff_rads = square(angle_cutoff_deg*M_PI/180.);
// static const double lum_cutoff = 7.e16;
// static const double lum_cutoff = 10.;

// create head node
maxlumtree::maxlumtree(double r_cent[3], double size) {
    for ( int ii=0 ; ii<3 ; ii++ ) {
        this->r_cent[ii] = r_cent[ii];
    }
    this->size = size;
    this->size2 = square(size);
    this->halfsize = size/2.;
    
//     this->nodes = new maxlumtree*[8];
//     for ( int ii=0 ; ii<8 ; ii++ ) {
//         this->nodes[ii] = NULL;
//     }
    
    this->ip = -1; // empty leaf node
    this->maxLum = -1;
    this->luminosity = 0.;
    this->listID = -1;
}

maxlumtree::~maxlumtree() {
    if ( nodeList ) {
        delete nodeList;
    }
}

void maxlumtree::buildNodeList() {
    nodeList = new std::vector<maxlumtree*>;
    
    this->propogateBuildNodeList(nodeList);
}

void maxlumtree::propogateBuildNodeList(std::vector<maxlumtree*> *nodeList) {
    this->listID = nodeList->size();
    nodeList->push_back(this);
    for ( int ii=0 ; ii<8 ; ii++ ) {
        if ( this->nodes[ii] ) {
            this->nodes[ii]->propogateBuildNodeList(nodeList);
        }
    }
}

double maxlumtree::calcLuminosities(struct SourceP_Type* allHotSourceP) {
    this->luminosity = 0.;

    for ( int ii=0 ; ii<8 ; ii++ ) {
        if ( this->nodes[ii] ) {
            this->luminosity+=this->nodes[ii]->calcLuminosities(allHotSourceP);
        }
    }
    
    if ( this->ip>=0 ) {
        this->luminosity+=allHotSourceP[this->ip].brightness*allHotSourceP[this->ip].transmitting_area;
    }
    return this->luminosity;
}

// void maxlumtree::propagateThreshList(double* allHotSourceP, double* r_p,std::vector<int>* threshList, int Ntot) {
void maxlumtree::propagateThreshList(struct SourceP_Type* allHotSourceP, double* r_p, double receiving_area, int Ntot,int *threshList,unsigned int *nThresh) {
    // Does this node fit the criterion?
    // If so - add my particle to the list if I'm a leaf node, or open up my child nodes if I'm a branch node
    
    // calculate distance
    double r2 = 0.;
    
    if ( this->ip<=-1 ) {
        // distance to closest edge of cell
        for ( int ii=0 ; ii<3 ; ii++ ) {
            r2+=square(std::max(std::fabs(r_p[ii]-this->r_cent[ii])-this->halfsize,0.));
        }
    } else {
        // distance to centre of particle I contain
        for ( int ii=0 ; ii<3 ; ii++ ) {
            r2+=square(r_p[ii]-allHotSourceP[this->ip].Pos[ii]);
        }
    }
    

    if ( r2>0. ) {
        // THIS IS THE KEY BIT
        double my_crit=this->maxLum*receiving_area/r2;

        if ( my_crit > lum_cutoff ) {
            if ( this->ip<=-1 ) {
//                 for ( int ii=0 ; ii<8 ; ii++ ) {
//                     if ( this->nodes[ii] ) {
//     //                     this->nodes[ii]->propagateThreshList(allHotSourceP,r_p,threshList,Ntot);
//                         this->nodes[ii]->propagateThreshList(allHotSourceP,r_p,receiving_area,Ntot,threshList,nThresh);
//                     }
//                 }


                // propagate to lower nodes if we're close, otherwise store this node
                double angle2 = size2/r2;
                if ( angle2>=angle2_cutoff_rads ) {
                    for ( int ii=0 ; ii<8 ; ii++ ) {
                        if ( this->nodes[ii] ) {
        //                     this->nodes[ii]->propagateThreshList(allHotSourceP,r_p,threshList,Ntot);
                            this->nodes[ii]->propagateThreshList(allHotSourceP,r_p,receiving_area,Ntot,threshList,nThresh);
                        }
                    }
                } else {
                    threshList[*nThresh]=-this->listID-1;
                    (*nThresh)++;
                }
            } else {
    //             threshList->push_back(this->ip);
                threshList[*nThresh]=this->ip;
                (*nThresh)++;
            }
        }
    } else {
        // We are inside the cell!
        if ( this->ip<=-1 ) {
            // propagate to lower nodes
            for ( int ii=0 ; ii<8 ; ii++ ) {
                if ( this->nodes[ii] ) {
//                     this->nodes[ii]->propagateThreshList(allHotSourceP,r_p,threshList,Ntot);
                    this->nodes[ii]->propagateThreshList(allHotSourceP,r_p,receiving_area,Ntot,threshList,nThresh);
                }
            }
        } else {
//             threshList->push_back(this->ip);
            threshList[*nThresh]=this->ip;
            (*nThresh)++;
        }
    }

    return;
}

// std::vector<int>* maxlumtree::getThreshList(double* allHotSourceP, double *r_p, int Ntot) {
//     std::vector<int>* threshList = new std::vector<int>;
//     
//     this->propagateThreshList(allHotSourceP,r_p,threshList,Ntot);
//     
//     return threshList;
// }

void maxlumtree::getThreshList(struct SourceP_Type* allHotSourceP, double *r_p, double receiving_area, int Ntot, int* threshList, unsigned int* nThresh) {
    (*nThresh)=0;
    this->propagateThreshList(allHotSourceP,r_p,receiving_area,Ntot,threshList,nThresh);
}

int maxlumtree::get_maxdepth() {
    return this->get_maxdepth(0,0);
}

int maxlumtree::get_maxdepth(int maxlevel,int ilevel) {
    for ( int inode=0 ; inode<8 ; inode++ ) {
        if ( this->nodes[inode] ) {
            maxlevel = std::max(maxlevel,this->nodes[inode]->get_maxdepth(maxlevel,ilevel+1));
        }
    }
    return std::max(maxlevel,ilevel);
}



void maxlumtree::dump() {
    this->dump(0);
}

void maxlumtree::dump(int ilevel) {
    for ( int ii=0 ; ii<ilevel ; ii++ ) {
        std::cout << ".";
    }
    std::cout << this->maxLum << " " << this->luminosity << " " << this->ip << std::endl;
    for ( int inode=0 ; inode<8 ; inode++ ) {
        if ( this->nodes[inode] ) {
            this->nodes[inode]->dump(ilevel+1);
        }
    }
}


void maxlumtree::placeInChildNode(double r_p[3], int ip, double lum) {
    int inode=0;
    for ( int ii=0 ; ii<3 ; ii++ ) {
        if ( r_p[ii]>this->r_cent[ii] ) {
            inode+=1<<ii;
        }
    }
    
    if ( !this->nodes[inode] ) {
        double r_cent[3];
        double quartersize = size/4;
        for ( int ii=0 ; ii<3 ; ii++ ) {
            if ( r_p[ii]>this->r_cent[ii] ) {
                r_cent[ii] = this->r_cent[ii]+quartersize;
            } else {
                r_cent[ii] = this->r_cent[ii]-quartersize;
            }
        }
        this->nodes[inode] = std::make_shared<maxlumtree>(r_cent,size/2.);
    }
    
    this->nodes[inode]->addp(r_p,ip,lum);
}

void maxlumtree::addp(double r_p[3], int ip, double lum) {
    if ( this->ip>=0 ) {
        // convert from leaf node to branch node
        
        this->placeInChildNode(this->r_p,this->ip, this->maxLum);
        this->ip = -2; // branch node
        
        this->placeInChildNode(r_p,ip,lum);
        this->maxLum = std::max(lum,this->maxLum);
    } else if ( this->ip==-1 ) {
        // add a particle to empty leaf node
        this->ip = ip;
        for ( int ii=0 ; ii<3 ; ii++ ) {
            this->r_p[ii] = r_p[ii];
        }
        this->maxLum = lum;
    } else {
        // branch node
        this->placeInChildNode(r_p,ip,lum);
        this->maxLum = std::max(lum,this->maxLum);
    }
}
