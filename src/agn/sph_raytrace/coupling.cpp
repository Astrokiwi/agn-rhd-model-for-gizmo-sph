/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "coupling.h"

#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>
#include <stdlib.h>     /* exit, EXIT_FAILURE */

#include "../../allvars.h"
extern "C"
{
#include "../../proto.h"
#include "../../kernel.h"
}

double opticalDepthCutoff() {
    return All.AGNOpticalDepthCutoff;
}
    
double kernel_wk(double r,double h) {
    if ( r>=h ) return 0.;

    // get kernel from GIZMO
    double hinv, hinv3, hinv4,wk,dwk;
    kernel_hinv(h, &hinv, &hinv3, &hinv4);
    kernel_main(r/h, hinv3, hinv4, &wk, &dwk, 0);

    return wk;        
}


double dtime_system(double t0, double t1) {
    return timediff(t0,t1);
}

double system_time(void) {
    return my_second();
}
