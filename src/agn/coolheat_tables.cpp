#include <mpi.h>
#include <stdlib.h>
#include <string>
#include <cstring>
#include <sstream>
#include <fstream>
#include "../allvars.h"
// extern "C"
// {
// #include "../proto.h"
// }
#include <stdio.h>

#include <iostream>
#include "sph_raytrace/rt_prototypes.h"
#include "sph_raytrace/raytrace_helpers.h"


struct AGN_heat_table {

    // heating/cooling tables under AGN
#ifdef COOLHEAT_TAB
    double *agn_heatcool_tab,*agn_arad_tab,*agn_opac_tab;
#else
    double *agn_heat_tab,*agn_cool_tab,*agn_arad_tab,*agn_opac_tab;
#endif
#ifdef SOTON_DUST_DUST_HEATING    
    double *agn_tdust_tab,*agn_dg_tab;
#endif
    int agn_ntemp,agn_ndense,agn_nintensity,agn_ntau;
    double *agn_temp_vals,*agn_dense_vals,*agn_intensity_vals,*agn_tau_vals;
    double *tables[4];
    int *ntabs[4];

    void setupTable(char labelFile[100],char tableFile[100]);
    
    int agn_tab_index(int id, int it, int ii, int is);
    
} mainTable,dustlessTable,densecoldTable;

/*! find index in (short) table where value fits to the right of
return -1 if it's to the left of everything, and n-1 if it's to the right of everything
*/
static inline int value_to_index(double edges[], int nedges, double value) {
    int i;
    
    if ( value<edges[0] ) return -1;
    for ( i=1 ; i<nedges ; i++ ) {
        if ( value<edges[i] ) {
            return i-1;
        }
    }
    return nedges-1;
}

/*! weighting for interpolation
No error checking!
*/
inline double right_weight(double edges[], int nedges, int index, double value) {
    if ( index<0 ) return 1.;
    if ( index>=nedges-1) return 0.;
    
    return (value-edges[index])/(edges[index+1]-edges[index]);
}

inline double asymmetric_intensity_factor(double *pos_p, double *pos_bh) {
#ifdef ASYMMETRIC_INTENSITY
    double agn_rot_x = pos_p[0]-pos_bh[0];
    double agn_rot_y = (pos_p[1]-pos_bh[1])*cos(All.AGN_theta)+(pos_p[2]-pos_bh[2])*sin(All.AGN_theta);
    double agn_rot_z =-(pos_p[1]-pos_bh[1])*sin(All.AGN_theta)+(pos_p[2]-pos_bh[2])*cos(All.AGN_theta);
    double agn_rad2 = square(agn_rot_x)+square(agn_rot_y)+square(agn_rot_z);

    // theta is co-latitude for some reason
    //double costheta_squared = square(P[i].Pos[2])/rad2;
    double costheta_squared = square(agn_rot_z)/agn_rad2;
    double costheta = sqrt(costheta_squared);
    //double asym_factor = costheta-2.*costheta_squared;
    double a=(All.AGN_isotropy-1.)/3.;
    double asym_factor = a*costheta+2.*a*costheta_squared+1.;
    double norm_factor = (7.*a/6.+1.);

    return asym_factor/norm_factor;
#else
    return 1;
#endif
}

int AGN_heat_table::agn_tab_index(int id, int it, int ii, int is) {
    return  is +
            agn_ntau*ii +
            agn_ntau*agn_nintensity*it +
            agn_ntau*agn_nintensity*agn_ntemp*id;
}


void AGN_heat_table::setupTable(char labelFile[100],char tableFile[100]) {

    int i,ntab;

    double junk;
    
#ifndef COOLHEAT_TAB
    double opac_scat, opac_abs;
#endif
    
    // read in tables
    std::ifstream f_tablab (labelFile, std::ifstream::in);
    
    if ( !f_tablab ) {
        std::cout << "AGN table labels file not found!" << std::endl;
        std::cout << labelFile << std::endl;
        MPI_Finalize();
        exit(0);            
    }

    f_tablab >> agn_ndense;
    agn_dense_vals = new double[agn_ndense];
    for ( i=0 ; i<agn_ndense ; i++ ) {
        f_tablab >> agn_dense_vals[i];
    }

    f_tablab >> agn_ntemp;
    agn_temp_vals = new double[agn_ntemp];
    for ( i=0 ; i<agn_ntemp ; i++ ) {
        f_tablab >> agn_temp_vals[i];
    }

    f_tablab >> agn_nintensity;
    agn_intensity_vals = new double[agn_nintensity];
    for ( i=0 ; i<agn_nintensity ; i++ ) {
        f_tablab >> agn_intensity_vals[i];
    }

    f_tablab >> agn_ntau;
    agn_tau_vals = new double[agn_ntau];
    for ( i=0 ; i<agn_ntau ; i++ ) {
        f_tablab >> agn_tau_vals[i];
    }
    All.AGNOpticalDepthCutoff = agn_tau_vals[agn_ntau-1];
    
    f_tablab.close();
    
    ntab=agn_ndense*agn_ntemp*agn_nintensity*agn_ntau;
#ifdef COOLHEAT_TAB    
    agn_heatcool_tab = new double[ntab];
#else
    agn_heat_tab = new double[ntab];
    agn_cool_tab = new double[ntab];
#endif
    
    agn_arad_tab = new double[ntab];
    agn_opac_tab = new double[ntab];
#ifdef SOTON_DUST_DUST_HEATING    
    agn_tdust_tab = new double[ntab];
    agn_dg_tab = new double[ntab];
#endif
    
    std::ifstream f_tabvals (tableFile, std::ifstream::in);

    if ( !f_tabvals ) {
        std::cout << "AGN table file not found!" << std::endl;
        std::cout << tableFile << std::endl;
        MPI_Finalize();
        exit(0);            
    }

#ifdef COOLHEAT_TAB    
    std::string dummyLine;
    getline(f_tabvals, dummyLine); // skip header line
#endif
    
    for ( i=0 ; i<ntab ; i++ ) {
        std::string in_line;
        std::getline(f_tabvals,in_line);
        std::stringstream line_stream(in_line);
#ifdef COOLHEAT_TAB    
        line_stream >> junk >> junk >> agn_arad_tab[i] >> junk >> agn_heatcool_tab[i] >> agn_opac_tab[i];
#else
#ifdef SOTON_DUST_DUST_HEATING    
        line_stream >> agn_heat_tab[i] >> agn_cool_tab[i] >> agn_tdust_tab[i] >> agn_arad_tab[i] >> agn_dg_tab[i] >> opac_scat >> opac_abs >> junk;
#else
        line_stream >> agn_heat_tab[i] >> agn_cool_tab[i] >> junk >> agn_arad_tab[i] >> junk >> opac_scat >> opac_abs >> junk;
#endif
        agn_opac_tab[i] = opac_scat+opac_abs;
        if ( agn_opac_tab[i]<0. ) {
            std::cout << "BAD OPACITY IN INPUT FILE " << tableFile << " line: " << i << " scattering:" << opac_scat
            << " absorption:" << opac_abs << " total:" << agn_opac_tab[i] << std::endl;
            exit(0);
        }
#endif
    }
    f_tabvals.close();
    
    //** Unit conversions
    
    // tau is unitless and doesn't need conversion
    
    // number density in cm**-3 to physical density in internal units
    const double molecular_mass = 4.0 / (1 + 3 * HYDROGEN_MASSFRAC);	/* note: assuming NEUTRAL GAS */
    double logdenseconvert = log10(
                                    (molecular_mass*PROTONMASS)      // from cm**-3 to g/cm**3
                                    /All.UnitDensity_in_cgs
                                    );
    for ( i=0 ; i<agn_ndense ; i++ ) {
        agn_dense_vals[i]+=logdenseconvert;
    }
    

    // temperature in K to internal energy in internal units
    double logtempconvert = log10(All.UnitMass_in_g / All.UnitEnergy_in_cgs*BOLTZMANN/GAMMA_MINUS1/(molecular_mass*PROTONMASS));
    for ( i=0 ; i<agn_ntemp ; i++ ) {
        agn_temp_vals[i]+=logtempconvert;
    }

    // intensity in erg/s/cm**2 to internal units
    double logintensityconvert = log10(square(All.UnitLength_in_cm)/All.UnitEnergy_in_cgs*All.UnitTime_in_Megayears*SEC_PER_MEGAYEAR);
    for ( i=0 ; i<agn_nintensity ; i++ ) {
        agn_intensity_vals[i]+=logintensityconvert;
    }

    // opacities in cm**2/g to internal units (e.g. kpc**2/1e10 Msun)
    double opacconvert = pow(All.UnitLength_in_cm,2)/All.UnitMass_in_g;
    for ( i=0 ; i<ntab ; i++ ) {
        agn_opac_tab[i]*=opacconvert;
    }
    
#ifdef COOLHEAT_TAB
    // heating/cooling from second to internal units (~Gyr)
    double heatcoolconvert = All.UnitTime_in_Megayears*SEC_PER_MEGAYEAR;
//     std::cout << "convert factor:" << All.UnitTime_in_Megayears*SEC_PER_MEGAYEAR << std::endl;
    for ( i=0 ; i<ntab ; i++ ) {
        agn_heatcool_tab[i]/=heatcoolconvert;
    }
#else
    // heating/cooling from erg/s/cm**3 to internal units energy/time/volume
    double heatcoolconvert = log10(pow(All.UnitLength_in_cm,3)/All.UnitEnergy_in_cgs*All.UnitTime_in_Megayears*SEC_PER_MEGAYEAR);
    for ( i=0 ; i<ntab ; i++ ) {
        agn_heat_tab[i]+=heatcoolconvert;
        agn_cool_tab[i]+=heatcoolconvert;
    }
#endif

    // rad pressure acceleration from cm/s/s to internal units kpc/time/time
    double aradconvert = log10(All.UnitLength_in_cm)-2.*log10(All.UnitTime_in_Megayears*SEC_PER_MEGAYEAR);
    for ( i=0 ; i<ntab ; i++ ) {
        agn_arad_tab[i]-=aradconvert;
    }
    
    // set up pointers for better looping
    tables[0] = agn_dense_vals;
    ntabs[0] = &agn_ndense;
    tables[1] = agn_temp_vals;
    ntabs[1] = &agn_ntemp;
    tables[2] = agn_intensity_vals;
    ntabs[2] = &agn_nintensity;
    tables[3] = agn_tau_vals;
    ntabs[3] = &agn_ntau;
    
}





// void setup_tables() {
//     mainTable.setupTable(All.AGNTabLabelFile,All.AGNTabFile);
//     dustlessTable.setupTable(All.AGNDustlessTabLabelFile,All.AGNDustlessTabFile);
//     densecoldTable.setupTable(All.AGNHighdenseTabLabelFile,All.AGNHighdenseTabFile);
// }

void setup_tables(
                    char AGNTabLabelFile[100], char AGNTabFile[100]
                    ,char AGNDustlessTabLabelFile[100], char AGNDustlessTabFile[100]
                    ,char AGNHighdenseTabLabelFile[100], char AGNHighdenseTabFile[100]
                  ) {
    mainTable.setupTable(AGNTabLabelFile,AGNTabFile);
    dustlessTable.setupTable(AGNDustlessTabLabelFile,AGNDustlessTabFile);
    densecoldTable.setupTable(AGNHighdenseTabLabelFile,AGNHighdenseTabFile);
}




#ifdef SOTON_AGN_PRES
double table_heat(int i, double *values) {
#else
void table_heat(int i, double *values) {
#endif    

    int jj,kk;
    
    int tab_index[4];
    int interp_indices[4][2];
    //int interp_right_index[4];
    double interp_weights[4][2];
    
    bool interpBetweenTables = false; // only use one table at a time

    
    AGN_heat_table *thisTable; 
    
    SphP[i].dusty = (SphP[i].InternalEnergy<=All.u_DustSput);

    if ( SphP[i].dusty ) {
        thisTable = &mainTable;
    } else {
        thisTable = &dustlessTable;
    }

    if ( values[0]>thisTable->agn_dense_vals[thisTable->agn_ndense-1] && values[1]<densecoldTable.agn_temp_vals[densecoldTable.agn_ndense-1] && SphP[i].dusty ) {
        if (values[0]>=densecoldTable.agn_dense_vals[0] ) {
            // only need to use cold low density table
            thisTable = &densecoldTable;
//             if ( ThisTask==0 ) std::cout << "using densecoldtable" << std::endl;
        } else {
            // need to interpolate between tables
            interpBetweenTables = true;
#ifdef COOLHEAT_TAB
            std::cout << "Integrated cooling/heating is not implemented for high density table" << std::endl;
            exit(0);
#endif
        }
    
    }
    

    for (jj=interpBetweenTables?1:0 ; jj<4 ; jj++ ) {
        tab_index[jj] = value_to_index(thisTable->tables[jj],*thisTable->ntabs[jj],values[jj]);
        interp_indices[jj][0] = std::max(tab_index[jj],0);
        interp_indices[jj][1] = std::min(tab_index[jj]+1,*thisTable->ntabs[jj]-1);
        interp_weights[jj][1] = right_weight(thisTable->tables[jj],*thisTable->ntabs[jj],tab_index[jj],values[jj]);
        interp_weights[jj][0] = 1.-interp_weights[jj][1];
//         if ( ThisTask==0 ) std::cout << jj << " " << tab_index[jj] << " " << interp_indices[jj][0] << " " << interp_indices[jj][1] << " " << interp_weights[jj][0]  << " " << interp_weights[jj][1] << std::endl;
    }
//     if ( ThisTask==0 ) std::cout << std::endl;

#ifdef COOLHEAT_TAB
    double coolheat_interp0=0.,arad_interp=0.,opac_interp=0.;
#else
    double heat_interp=0.,cool_interp=0.,arad_interp=0.,opac_interp=0.;
#endif

#ifdef SOTON_DUST_DUST_HEATING    
    double tdust_interp=0.,dg_interp=0.;
#endif
    
    if ( interpBetweenTables ) {
//         if ( ThisTask==0 ) std::cout << "interpbetweentables" << std::endl;
        interp_indices[0][0] = thisTable->agn_ndense-1;
        interp_indices[0][1] = 0;
        interp_weights[0][1] = (values[0]-thisTable->agn_dense_vals[thisTable->agn_ndense-1])
                                /(densecoldTable.agn_dense_vals[0]-thisTable->agn_dense_vals[thisTable->agn_ndense-1]);
        interp_weights[0][0] = 1.-interp_weights[0][1];
        
        int interp_indices_lowtemp[2];
        if ( interp_indices[1][1]>densecoldTable.agn_ntemp-1 ) {
            for (jj=0 ; jj<2 ; jj++) {
                interp_indices_lowtemp[jj] = densecoldTable.agn_ntemp-1;
            }
        } else {
            for (jj=0 ; jj<2 ; jj++) {
                interp_indices_lowtemp[jj] = interp_indices[1][jj];
            }
        }
        
        AGN_heat_table *curTable;
        for (jj=0 ; jj<16 ; jj++ ) {
            int idex;
            
            if ( jj%2==0 ) {
                curTable = thisTable;
                idex = curTable->agn_tab_index(interp_indices[0][jj%2],interp_indices[1][(jj/2)%2],interp_indices[2][(jj/4)%2],interp_indices[3][(jj/8)%2]);
            } else {
                curTable = &densecoldTable;
                idex = curTable->agn_tab_index(interp_indices[0][jj%2],interp_indices_lowtemp[(jj/2)%2],interp_indices[2][(jj/4)%2],interp_indices[3][(jj/8)%2]);
            }

            double weight = 1.;
            for ( kk=0 ; kk<4 ; kk++ ) {
                weight*=interp_weights[kk][(jj/(1<<kk))%2];
            }
//             if ( ThisTask==0 ) std::cout << curTable << " " << idex << " " << weight << std::endl;

            heat_interp+=weight*curTable->agn_heat_tab[idex];
            cool_interp+=weight*curTable->agn_cool_tab[idex];
            arad_interp+=weight*curTable->agn_arad_tab[idex];
            opac_interp+=weight*curTable->agn_opac_tab[idex];

#ifdef SOTON_DUST_DUST_HEATING    
            tdust_interp+=weight*curTable->agn_tdust_tab[idex];
            dg_interp   +=weight*curTable->agn_dg_tab[idex];
#endif
        }
//         if ( ThisTask==0 ) std::cout << cool_interp << " " << heat_interp << std::endl;
//         if ( ThisTask==0 ) std::cout << cool_interp << " " << heat_interp << std::endl;
    } else {
    //     double weightsum = 0.;
        for (jj=0 ; jj<16 ; jj++ ) {
            int idex = thisTable->agn_tab_index(interp_indices[0][jj%2],interp_indices[1][(jj/2)%2],interp_indices[2][(jj/4)%2],interp_indices[3][(jj/8)%2]);
            //int idex = agn_tab_index(5,5,interp_indices[2][(jj/4)%2],5);
            double weight = 1.;
            for ( kk=0 ; kk<4 ; kk++ ) {
                weight*=interp_weights[kk][(jj/(1<<kk))%2];
            }
            //weight = interp_weights[2][(jj/4)%2];
//             if ( ThisTask==0 ) std::cout << idex << " " << weight << std::endl;
        
#ifdef COOLHEAT_TAB
            coolheat_interp0+=weight*thisTable->agn_heatcool_tab[idex];
#else
            heat_interp+=weight*thisTable->agn_heat_tab[idex];
            cool_interp+=weight*thisTable->agn_cool_tab[idex];
#endif
            arad_interp+=weight*thisTable->agn_arad_tab[idex];
            opac_interp+=weight*thisTable->agn_opac_tab[idex];
#ifdef SOTON_DUST_DUST_HEATING    
            tdust_interp+=weight*thisTable->agn_tdust_tab[idex];
            dg_interp   +=weight*thisTable->agn_dg_tab[idex];
#endif
        }
//         if ( ThisTask==0 ) std::cout << cool_interp << " " << heat_interp << std::endl;
    
    }
#ifndef COOLHEAT_TAB
    // do extrapolation for density above max
    if ( values[0]>thisTable->agn_dense_vals[thisTable->agn_ndense-1] && !interpBetweenTables) {
//         if ( ThisTask==0 ) std::cout << "extrapolation" << std::endl;
        heat_interp += values[0]-thisTable->agn_dense_vals[thisTable->agn_ndense-1]; // heating per unit volume is proportional to density - i.e. heating per unit mass is constant
        cool_interp += 2.*(values[0]-thisTable->agn_dense_vals[thisTable->agn_ndense-1]); // cooling per volume is proportional to n^2
    }
    
    // do extrapolation for intensity above max - assume heating is proportional to intensity outside of the table
    // do extrapolation for intensity below minimum
    if ( values[2]<thisTable->agn_intensity_vals[0] ) {
        heat_interp += values[2] - thisTable->agn_intensity_vals[0];
    }
    // do extrapolation for intensity above maximum
    if ( values[2]>thisTable->agn_intensity_vals[thisTable->agn_nintensity-1] ) {
        heat_interp += values[2] - thisTable->agn_intensity_vals[thisTable->agn_nintensity-1];
    }

    // tables are in energy/time/volume, divide by density to get energy/time/mass
    double heating = pow(10.,heat_interp);
    double cooling = pow(10.,cool_interp);
    
    
    SphP[i].DtEnergyAGN = (heating-cooling)/SphP[i].Density;
//     MPI_Barrier(MPI_COMM_WORLD);
//     exit(0);
#else
    // integrated method - intended to give a bigger timestep but gives mixed results and temperatures tend to stick to certain peaks - need some sort of fancy interpolation etc

    // set heating/cooling using pre-integrated arrays so we don't need a timestep limiter
    //int itemp_low=interp_indices[1][0],itemp_high=interp_indices[1][1];
    double heat_interp_weights[3][2];
    int indices[3] = {0,2,3};
//    double temp_weight = interp_weights[1][0]*interp_weights[1][0]+2.*interp_weights[1][0]*interp_weights[1][1]+interp_weights[1][1]*interp_weights[1][1];
    // calculate weights, summing over both temperature sides
    for ( dd=0 ; dd<2 ; dd++ ) {
        for ( kk=0 ; kk<3 ; kk++ ) {
            int ii = indices[kk];
            heat_interp_weights[kk][dd] = interp_weights[ii][dd];//*temp_weight;
        }
    }

    
    // search to find integrated temperature timescale that fits target
    // build the integrated table first, because it's not got that many points in it
    double coolheat_interp_here;

    double p_dt = (P[i].TimeBin ? (((integertime) 1) << P[i].TimeBin) : 0) * All.Timebase_interval;
//     if ( All.NumCurrentTiStep==0 ) {
//         p_dt = 1.e-7; // ~100 years of equilibration
//     }
    if ( p_dt>0. ) {
//         p_dt = 1.; // for DEBUG
        double target_coolheat = coolheat_interp0 + p_dt;
//         std::cout << "coolheat:" << coolheat_interp0 << " " << p_dt << " " << target_coolheat << std::endl;
//         exit(0);
    // for dump
        const double molecular_mass = 4.0 / (1 + 3 * HYDROGEN_MASSFRAC);	/* note: assuming NEUTRAL GAS */
        double logtempconvert = log10(All.UnitMass_in_g / All.UnitEnergy_in_cgs*BOLTZMANN/GAMMA_MINUS1/(molecular_mass*PROTONMASS));

        // build new interpolated table, then search it
        double interp_tab[thisTable->agn_ntemp];
        for (int itemp_search=0 ; itemp_search<thisTable->agn_ntemp ; itemp_search++ ) {
            // do interpolation
            coolheat_interp_here=0;
            double totalweight=0.;
            for (jj=0 ; jj<8 ; jj++ ) {
                int idex = thisTable->agn_tab_index(interp_indices[0][jj%2],itemp_search,interp_indices[2][(jj/2)%2],interp_indices[3][(jj/4)%2]);
                double weight = 1.;
                for ( kk=0 ; kk<3 ; kk++ ) {
                    weight*=heat_interp_weights[kk][(jj/(1<<kk))%2];
                }
                coolheat_interp_here+=weight*thisTable->agn_heatcool_tab[idex];
            }
            interp_tab[itemp_search] = coolheat_interp_here;
        }
        

        // now search table

        int search_low = interp_indices[1][0];
        int search_high = interp_indices[1][1];
        int ilow=-1,ihigh = -1;
    
        bool finished_search = false;
        bool crossed_low,crossed_high;


        while (!finished_search) {
            if ( search_low>0 ) {
                crossed_low = interp_tab[search_low]>target_coolheat;
            } else crossed_low = false;
        
            if ( search_high<thisTable->agn_ntemp ) {
                crossed_high = interp_tab[search_high]>target_coolheat;
            } else crossed_high = false;
        
        
            if ( search_low<0 && search_high>=thisTable->agn_ntemp ) {
                finished_search = true;
                // this means we have enough time to reach the global equilibrium for these parameters
            }

            if ( crossed_low && crossed_high ) {
                // we're at an unstable point - temperature could either increase or decrease from here
                // Do check here to see which is closer
                if ( interp_tab[search_low]-target_coolheat>interp_tab[search_high]-target_coolheat ) {
                    ilow = search_high-1;
                } else {
                    ilow = search_low;
                }
                ihigh = ilow+1;
                finished_search = true;
            } else if ( crossed_low ) {
                ilow = search_low;
                ihigh = search_low+1;
                finished_search = true;
            } else if ( crossed_high ) {
                ilow = search_high-1;
                ihigh = search_high;
                finished_search = true;
            } else {
                search_low--;
                search_high++;
            }
        }
    
        // apply temperatures, using interpolation when necessary
        double log_target_temp;
        if ( ilow==-1 && ihigh==-1 ) {
            // we have enough time to reach equilibrium - go to the minimum point in the array
            int imax = 0;
            for ( int ii=1 ; ii<thisTable->agn_ntemp ; ii++ ) {
                if ( interp_tab[ii]>interp_tab[imax] ) {
                    imax=ii;
                }
            }
            // if max is at ends of table, can't do interpolate - just use bounds
            if ( imax==0 || imax==thisTable->agn_ntemp-1 ) {
                log_target_temp = thisTable->agn_temp_vals[imax];
            } else {
                int ileft;
                if ( interp_tab[imax-1]>interp_tab[imax+1] ) {
                    ileft = imax-1;
                } else {
                    ileft = imax;
                }
                if ( interp_tab[ileft]<0. && interp_tab[ileft+1]<0. ) {
                    log_target_temp = (thisTable->agn_temp_vals[ileft]+thisTable->agn_temp_vals[ileft+1])/2.;
                } else if ( interp_tab[ileft]<0. ) {
                    log_target_temp = thisTable->agn_temp_vals[ileft+1];
                } else if ( interp_tab[ileft+1]<0. ) {
                    log_target_temp = thisTable->agn_temp_vals[ileft];
                } else if ( interp_tab[ileft]==interp_tab[ileft+1] ) {
                    // divide by zero if left & right equal, but the limit is that we are half way inbetween
                    log_target_temp = (thisTable->agn_temp_vals[ileft]+thisTable->agn_temp_vals[ileft+1])/2.;
                } else {
                    // math on 16/7/18 of my logbook (last page!)
                    log_target_temp = (thisTable->agn_temp_vals[ileft]*interp_tab[ileft]-thisTable->agn_temp_vals[ileft+1]*interp_tab[ileft+1]
                                          + (thisTable->agn_temp_vals[ileft+1]-thisTable->agn_temp_vals[ileft])*
                                             sqrt(interp_tab[ileft]*interp_tab[ileft+1])
                                       )/(interp_tab[ileft]-interp_tab[ileft+1]);
                }
            }
        } else {
            if ( ilow<0 ) {
                // do extrapolation too in future
                log_target_temp = thisTable->agn_temp_vals[0];
            } else if ( ihigh>=thisTable->agn_ntemp ) {
                // do extrapolation too in future
                log_target_temp = thisTable->agn_temp_vals[thisTable->agn_ntemp-1];
            } else {
                double interpf = right_weight(interp_tab,thisTable->agn_ntemp,ilow,target_coolheat);
                log_target_temp = thisTable->agn_temp_vals[ilow]*(1.-interpf)+thisTable->agn_temp_vals[ihigh]*interpf;
            }
        }
        //SphP[i].InternalEnergy = pow(10.,log_target_temp);
        SphP[i].DtEnergyAGN = (pow(10.,log_target_temp)-SphP[i].InternalEnergy)/p_dt;
//         std::cout << ThisTask << "Result:" << ilow << " " << ihigh << " " << interpf << " " << log_target_temp << " Final:" << SphP[i].DtEnergyAGN << std::endl;
//         exit(0);
    } else { // if dt<=0
        SphP[i].DtEnergyAGN = 0.;
    }

#endif




//     SphP[i].DtEnergyAGN = p_dt;

//     double min_table_dist = 1000000;
//     double table_float_index = interp_indices[1][0]*interp_weights[1][0]+interp_indices[1][1]*interp_weights[1][1];
//     int min_table_index = -1;
//     int itemp_search;
//        double dist = std::fabs(coolheat_interp_here-target_coolheat);
//         double table_dist = std::fabs((double)itemp_search-table_float_index);
//         if ( coolheat_interp_here>target_coolheat && table_dist<min_table_dist ) {
//             min_table_dist = table_dist;
//         }
    


    // tables are in energy/time/volume, divide by density to get energy/time/mass
//     double heating = pow(10.,heat_interp);
//     double cooling = pow(10.,cool_interp);
    
    
//     SphP[i].DtEnergyAGN = (heating-cooling)/SphP[i].Density;
    //SphP[i].DtEnergyAGN = (pow(10.,cool_interp))/SphP[i].Density;
    
    //SphP[i].Opacity = pow(10.,opac_interp);
    SphP[i].Opacity = opac_interp;
    


#ifdef SOTON_DUST_DUST_HEATING
    SphP[i].tdust=tdust_interp;
    SphP[i].dg=dg_interp;
#endif
#ifdef SOTON_AGN_PRES
    //double radforce_mag = heating / (C / All.UnitVelocity_in_cm_per_s);

    // extrapolation - assume radiation pressure is proportional to intensity outside of the table
    // do extrapolation for intensity below minimum
    if ( values[2]<thisTable->agn_intensity_vals[0] ) {
        arad_interp += values[2] - thisTable->agn_intensity_vals[0];
    }
    // do extrapolation for intensity above maximum
    if ( values[2]>thisTable->agn_intensity_vals[thisTable->agn_nintensity-1] ) {
        arad_interp += values[2] - thisTable->agn_intensity_vals[thisTable->agn_nintensity-1];
    }
    return arad_interp;
#endif
}















/*! calculate heating and cooling rates from table
*/
void agn_calc_heat_cool_p_raytraced_tab(int i) {
    int jj;
    

    double rad2=0.;
#ifdef BINARY_BH
    double rad2_2=0.;
    double switch_on_factor=1;
#endif
    
    double values[4];
    double arad_interp;
    
    
    for ( jj=0 ; jj<3 ; jj++ ) {
#ifdef BINARY_BH
        rad2 += square(P[i].Pos[jj]-All.BH_binary.Pos_1[jj]);
        rad2_2 += square(P[i].Pos[jj]-All.BH_binary.Pos_2[jj]);
#else
        rad2 += square(P[i].Pos[jj]);
#endif
    }



#ifdef BINARY_BH
    if (All.Time<All.binary_time) {
        // zero intensity breaks things - it gets logged below
        // so it linearly scales from 1% to 100% over the time-scale
        // TODO: put the "1%" in an input parameter?
        switch_on_factor = 0.01+0.99*All.Time/All.binary_time;
    }

    SphP[i].Intensity = (1./4./M_PI) * All.AGN_L/rad2 * asymmetric_intensity_factor(P[i].Pos,All.BH_binary.Pos_1) * switch_on_factor;
    SphP[i].Intensity_2 = (1./4./M_PI) * All.AGN_L_2/rad2_2 * asymmetric_intensity_factor(P[i].Pos,All.BH_binary.Pos_2) * switch_on_factor;
#else
    double agn_p[3] = {0,0,0};
    SphP[i].Intensity = (1./4./M_PI) * All.AGN_L/rad2 * asymmetric_intensity_factor(P[i].Pos,agn_p);
#endif

    values[0] = log10(SphP[i].Density); // density
    values[1] = log10(SphP[i].InternalEnergy); // temperature

#ifdef BINARY_BH
//     if ( SphP[i].Intensity>SphP[i].Intensity_2 ) {
//         SphP[i].Intensity_Effective = log10(SphP[i].Intensity + SphP[i].Intensity_2*exp(SphP[i].OpticalDepth - SphP[i].OpticalDepth_2));
//     } else {
//         SphP[i].Intensity_Effective = log10(SphP[i].Intensity_2 + SphP[i].Intensity*exp(SphP[i].OpticalDepth_2 - SphP[i].OpticalDepth));
//     }
    SphP[i].OpticalDepth_Effective =             (SphP[i].Intensity + SphP[i].Intensity_2)
                                                                    /
                            (SphP[i].Intensity/SphP[i].OpticalDepth + SphP[i].Intensity_2/SphP[i].OpticalDepth_2); // Rosseland mean
    

    double intensity_weight_1 =  exp( SphP[i].OpticalDepth_Effective - SphP[i].OpticalDepth  );
    double intensity_weight_2 =  exp( SphP[i].OpticalDepth_Effective - SphP[i].OpticalDepth_2);
    
    SphP[i].Intensity_Effective =         SphP[i].Intensity * intensity_weight_1 
                                        + SphP[i].Intensity_2   * intensity_weight_2;

    if ( SphP[i].OpticalDepth_Effective>All.AGNOpticalDepthCutoff ) {
        SphP[i].Intensity_Effective*=exp(All.AGNOpticalDepthCutoff-SphP[i].OpticalDepth_Effective);
        SphP[i].OpticalDepth_Effective=All.AGNOpticalDepthCutoff;
    }

    values[2] = log10(SphP[i].Intensity_Effective);
    values[3] = SphP[i].OpticalDepth_Effective;
#else
    values[2] = log10(SphP[i].Intensity); // unextinguished intensity
    values[3] = SphP[i].OpticalDepth; // optical depth
#endif
    

#ifdef SOTON_AGN_PRES
    arad_interp=table_heat(i,values);

    double radforce_mag = pow(10.,arad_interp);
    double rad = sqrt(rad2);

#ifdef BINARY_BH
//     double radforce_mag_1= radforce_mag * SphP[i].Intensity / (SphP[i].Intensity + SphP[i].Intensity_2);
//     double radforce_mag_2= radforce_mag * SphP[i].Intensity_2 / (SphP[i].Intensity + SphP[i].Intensity_2);
    double radforce_weighted_1 = SphP[i].Intensity  *exp(SphP[i].OpticalDepth_Effective-SphP[i].OpticalDepth);
    double radforce_weighted_2 = SphP[i].Intensity_2*exp(SphP[i].OpticalDepth_Effective-SphP[i].OpticalDepth_2);
    double radforce_binary_norm = radforce_weighted_1+radforce_weighted_2;
    double radforce_mag_1 = radforce_mag * radforce_weighted_1 / radforce_binary_norm;
    double radforce_mag_2 = radforce_mag * radforce_weighted_2 / radforce_binary_norm;
    double rad_2 = sqrt(rad2_2);
    for ( jj=0 ; jj<3 ; jj++ ) {
          SphP[i].RadAccel[jj] = radforce_mag_1 * (P[i].Pos[jj]-All.BH_binary.Pos_1[jj])/rad + radforce_mag_2 * (P[i].Pos[jj]-All.BH_binary.Pos_2[jj])/rad_2;
    }
#else
    for ( jj=0 ; jj<3 ; jj++ ) {
       SphP[i].RadAccel[jj] = radforce_mag*P[i].Pos[jj] / rad;
    }
#endif

#else // i.e. if AGN rad pressure is off
    table_heat(i,values);
#endif
}
