#include <mpi.h>
#include "../allvars.h"
extern "C"
{
#include "../proto.h"
}
#include <iostream>
#include <limits>
#include <tuple>
#include <string>

#include <fstream>


#include "cpp_prototypes.h"
#include "cpp_helpers.h"

// these are only accessible locally in this file
// i.e. it makes things act like a Fortran module
// This is just so we can split up our function into pieces that share the same data
// without having to create a static class
namespace {
    struct SourceP_Type *localHotSourceP;
    struct SourceP_Type *allHotSourceP;
    int *allHotSourceIndex;
//     int localHotSourceOffset;

    int NsourceP;
//     int sourceP_Ndata;

    double IR_depth_cutoff = 5.; //10., 50.
    double IR_strength_cutoff = 1.e-4;//1.e-5; // L/r^2*exp(-tau) cutoff - only if tau>IR_depth_cutoff too
    double IR_source_cutoff = 1.e-1;// source luminosity must be above this to even be considered a source
//     double IR_strength_cutoff = 0.;
}

// int get_sourceP_Ndata() {
//     return sourceP_Ndata;
// }




void dust_dust_packgather_hot() {
    int i,j,ic;
    // MPI sharing
    int numpartlist[NTask], numpartdisplacements[NTask];
    
    int allLocalSourceIndex[NumPart];

    // consts that are spread around everywhere because we suck
//     const double molecular_mass = 4.0 / (1 + 3 * HYDROGEN_MASSFRAC);    /* note: assuming NEUTRAL GAS */
//     const double K_TO_U = All.UnitMass_in_g / All.UnitEnergy_in_cgs*BOLTZMANN/GAMMA_MINUS1/(molecular_mass*PROTONMASS);
    // store/tabulate somewhere
    double grain_surface_per_gram = 262.; // cm**2/g
    grain_surface_per_gram*=All.UnitMass_in_g/pow(All.UnitLength_in_cm, 2);


    j=0;
    for (i=0 ; i<NumPart ; i++ ) {
        if( P[i].Type==0 && P[i].Mass > 0 ) {
            for ( ic=0 ; ic<3 ; ic++ ) {
                localHotSourceP[i].Pos[ic] = P[i].Pos[ic];
            }
            allLocalSourceIndex[i] = i;
            localHotSourceP[i].brightness = All.SB_internal*square(square(SphP[i].tdust)); // T**4, unnormalised
            localHotSourceP[i].transmitting_area = SphP[i].dg*std::min(grain_surface_per_gram*P[i].Mass,4.*M_PI*square(P[i].Hsml)); // effective transmitting area
            localHotSourceP[i].Hsml = P[i].Hsml; // for extinction
            localHotSourceP[i].Mass = P[i].Mass; // for extinction
            j++;
        }
    }

    MPI_Allgather(&j, 1, MPI_INT, &(numpartlist[0]), 1, MPI_INT, MPI_COMM_WORLD);

    NsourceP = 0;
    for (i=0 ; i<NTask ; i++ ) {
        NsourceP+=numpartlist[i];
    }

    numpartdisplacements[0] = 0;
    for (i=1 ; i<NTask ; i++ ) {
        numpartdisplacements[i]=numpartdisplacements[i-1]+numpartlist[i-1];
    }
    MPI_Allgatherv(allLocalSourceIndex, NumPart, MPI_INT, allHotSourceIndex, numpartlist, numpartdisplacements, MPI_INT, MPI_COMM_WORLD);
    

    for (i=0 ; i<NTask ; i++ ) {
        numpartlist[i]=numpartlist[i]*sizeof(SourceP_Type); // update coordinates per particle
    }
    numpartdisplacements[0] = 0;
    for (i=1 ; i<NTask ; i++ ) {
        numpartdisplacements[i]=numpartdisplacements[i-1]+numpartlist[i-1];
    }
    MPI_Allgatherv(&(localHotSourceP)[0], NumPart*sizeof(SourceP_Type), MPI_BYTE, &(allHotSourceP)[0], numpartlist, numpartdisplacements, MPI_BYTE, MPI_COMM_WORLD);


}


std::tuple<std::shared_ptr<maxlumtree>,std::shared_ptr<absorbtree>> dust_dust_buildtrees() {
    int i,ic;
//     const double molecular_mass = 4.0 / (1 + 3 * HYDROGEN_MASSFRAC);	/* note: assuming NEUTRAL GAS */
//     const double K_TO_U = All.UnitMass_in_g / All.UnitEnergy_in_cgs*BOLTZMANN/GAMMA_MINUS1/(molecular_mass*PROTONMASS);

    double rmin[3],rmax[3],size;
    std::shared_ptr<maxlumtree> maxTree;
    std::shared_ptr<absorbtree> bigTree;

    // recalculate "size" to have *all* emitting particles, not just local absorbing particles
    for ( ic=0 ; ic<3 ; ic++ ) {
        rmin[ic] = 0.;
        rmax[ic] = 0.;
    }
    for ( i=0 ; i<NsourceP ; i++ ) {
        for ( ic=0 ; ic<3 ; ic++ ) {
            if ( allHotSourceP[i].Pos[ic]<rmin[ic] ) {
                rmin[ic] = allHotSourceP[i].Pos[ic];
            }
            if ( allHotSourceP[i].Pos[ic]>rmax[ic] ) {
                rmax[ic] = allHotSourceP[i].Pos[ic];
            }
        }
    }
        
    size=rmax[0]-rmin[0];
    for ( int ic=1 ; ic<3 ; ic++ ) {
        size = std::max(size,rmax[ic]-rmin[ic]);
    }
    
    // build the minimum luminosity oct tree
    double r_cent[3];
    for ( ic = 0 ; ic<3 ; ic++ ) {
        r_cent[ic] = rmin[ic]+size/2.;
    }

    // box contains [rmin,rmin+size)
    // so must increase size by a small amount so that the "rightmost"
    // particle actually fits strictly inside
    size*=(1.+1.e-6);
    
    
    // is this expensive compared to the raytracing? is there a better way to update the tree?
    maxTree = std::make_shared<maxlumtree>(r_cent,size);
    int nActualSources = 0;
    for ( i=0 ; i<NsourceP ; i++ ) {
//         mintree->addp(&allHotSourceP[i*sourceP_Ndata],i,All.heatEmit*square(allHotSourceP[i*sourceP_Ndata+3])*allHotSourceP[i*sourceP_Ndata+4]/M_PI);
        double luminosity = allHotSourceP[i].brightness*allHotSourceP[i].transmitting_area;
        if ( luminosity>=IR_source_cutoff ) {
            maxTree->addp(&(allHotSourceP[i].Pos[0]),i,luminosity);
            nActualSources++;
        }
    }
    
    maxTree->calcLuminosities(allHotSourceP);
    maxTree->buildNodeList();
    
    // loop through nodelist and dump!
    // -> check on locations too? (centre of luminosity?)
//     if ( ThisTask==0 ) {
//         int maxlevel = maxTree->get_maxdepth();
//         double fullSize = maxTree->size;
//         for ( int ilevel=0 ; ilevel<=maxTree->get_maxdepth() ; ilevel ++ ) {
// //         for ( int ilevel=0 ; ilevel<=5 ; ilevel ++ ) {
//             double levelLum = 0.;
//             for ( maxlumtree *node : *maxTree->nodeList ) {
//                 int level = log2(fullSize/node->size);
//                 if ( level==ilevel ) {
//                     levelLum+=node->luminosity;
// //                     std::cout << level << " " << node->luminosity << std::endl;
//                 }
//             }
//             std::cout << "SUM:" << ilevel << " " << levelLum << std::endl;
//         }
//     }
//     
//     MPI_Barrier(MPI_COMM_WORLD);
//     exit(0);
    
//     std::cout << ThisTask << " Sources:" << nActualSources << "/" << NsourceP << std::endl;
    
    bigTree = std::make_shared<absorbtree>(rmin,size);
    for(i = 0; i < NsourceP; i++) {
//         if ( allHotSourceP[i*sourceP_Ndata+3]*K_TO_U<=All.u_DustSput ) {
            bigTree->head_addP(i,&(allHotSourceP[i].Pos[0]),allHotSourceP[i].Hsml);
//         }
    }
//     if ( ThisTask==0 ) std::cout << "A mintree " << mintree.use_count() << " bigTree " << bigTree.use_count() << std::endl; 

    return std::make_tuple(maxTree,bigTree);
    //return {mintree,bigTree};
}

/* the loops that:
    - determine which "source" particles are above the threshold to cause significant rad pressure if optical depth is small (mintree->getThreshList)
    - determine the actual optical depth between each >threshold source particle (one_IR_tree_ray)
    - calculate force & heating on each pair as a function of that stuff */
// can we make this fasterer?
std::tuple<double *, double *> dust_dust_rays(int localNActive,std::shared_ptr<maxlumtree> maxTree,std::shared_ptr<absorbtree> bigTree,int localOffset,int localIndex[],bool alreadyDone[]) {
//     struct rayBlock {
//         int sourceP;
//         double targetCoords[3];
//         double receivingArea;
//     };

    double *IR_absorbed = new double[localNActive];
    double *IR_force = new double[localNActive*3];
    
    int trueLocalNActive = localNActive;
    
    unsigned int sourceChunkSize = 100000; // build up this many rays before sharing
//     unsigned int sourceChunkSizes[] = {10000,100000,10000000};
//     int NActiveDivisors[] = {1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192};
    std::string time_label[] = {"Total","Build","Wait1","Comm1","Ray","Wait2","Gather2","Scatter2","Force"};
    int ntimes = 9;
    if ( ThisTask==0 ) {
        std::cout << "#Nprocs Chunksize NActiveTot NInteractionsTot ";
        for ( int itime=0 ; itime<ntimes ; itime++ ) {
            std::cout << time_label[itime] << " ";
        }
        std::cout << std::endl;
    }

//     for (unsigned int sourceChunkSize : sourceChunkSizes ) {
//     for ( int NActiveDivisor : NActiveDivisors ) {
//     localNActive=trueLocalNActive/NActiveDivisor;

    
//     struct *sendingRays = new rayBlock[sourceChunkSize*2];
    int *localRaySourceList = new int[sourceChunkSize*2]; // list of hot source particles - everybody knows their locations (oversized array to avoid overflow)
    double *localRayTargetCoords = new double[3*sourceChunkSize*2]; // list of locations of targets - not everybody has every particle
    double *localRayReceivingAreas = new double[sourceChunkSize*2]; // list of receiving areas for targets
    unsigned int *localNThresh = new unsigned int[sourceChunkSize]; // will be much smaller than sourceChunkSize in practice
    
    int *localTargetIDs = new int[sourceChunkSize];  // will be much smaller than sourceChunkSize in practice
    int *localTargetActiveIDs = new int[sourceChunkSize];  // will be much smaller than sourceChunkSize in practice
    
    int *todoRaySourceList = new int[sourceChunkSize];
    double *todoRayTargetCoords = new double[3*sourceChunkSize];
    double *todoRayReceivingAreas = new double[sourceChunkSize];
    double *returnDepths = new double[sourceChunkSize];

    double *returnedDepths = new double[sourceChunkSize];
    
    // constant that should really be defined elsewhere
//     double dustDustAbsorbT = 1000.; // cm**2/g
//     double dustDustAbsorbT = 262.; // cm**2/g
//     dustDustAbsorbT*=All.UnitMass_in_g/pow(All.UnitLength_in_cm, 2); // area/mass * (temperature of emitter in K)**2 (i.e. divide this by T(K)**2 to get area/mass)..?

    double depthCrossSection = 262.*All.UnitMass_in_g/pow(All.UnitLength_in_cm, 2);//dustDustAbsorbT/square(allHotSourceP[i*sourceP_Ndata+3]);
//     double grain_surface_per_gram = 261.; // cm**2/g
//     grain_surface_per_gram*=All.UnitMass_in_g/pow(All.UnitLength_in_cm, 2);


    for(int j = 0; j < localNActive; j++) {
        IR_absorbed[j] = 0.;
        for ( int ic=0 ; ic<3 ; ic++ ) {
            IR_force[j*3+ic] = 0.;
        }
    }
    // For each local point, find all particles that are "hot" enough to potentially cause significant heating/pressure
    // Calculate the depth/column density to each of those "hot" particles
    // Then add the heating and pressure from that particle
    
    // For load balance, do this in chunks.
    //
    // Stage 1
    // Every node loops through its own particles, using the maxlumtree to calculate how many rays need to be calculated
    // for that particle (i.e. how many "hot" particles matter for that target particle), building a list of these sources & targets
    // Once the number of rays is equal to the chunk size, or we've run out of local target particles, we move onto the next stage
    // (This part should run fast - the tree is quite efficient)
    //
    // Stage 2-4
    // We gather all the rays on the root node, and then scatter the rays evenly across all nodes
    // Then we calculate the optical depth along each ray, and share that back via the root node
    // We also share whether we've all finished or not
    //
    // Stage 5
    // If I received any optical depths, do the calculations for force/heating from the hot particles through that optical depth
    //
    // If we noted earlier that we're all finished, we then go back to Stage 1, otherwise, let's roll on back

    bool allDone = false;
    double t0,t1;
    t0  = my_second();
    double t_then,t_now;
    unsigned int n_interactions = 0;
    bool localDone = false;
    int local_particle_j = 0;

    double build_time = 0.;
    double comm_time1 = 0.;
    double wait_time1 = 0.;
    double ray_time = 0.;
    double comm_time2 = 0.;
    double wait_time2 = 0.;
    double force_time = 0.;
    double gather_time2 = 0.;
    double scatter_time2 = 0.;
    
    unsigned int nits = 0;

    while (!allDone ) {
//         if ( ThisTask==0 ) {
//             std::cout << "ITERATION STARTED" << nits << std::endl;
//         }
        unsigned int localNRays = 0;
        unsigned int NtodoRays = 0;
        unsigned int localNTargets = 0;
        t_then = my_second();
        
        // Stage 1 - build up chunks
        while ( local_particle_j<localNActive ) {
            int ip = localIndex[local_particle_j];
        
            double receiving_area = SphP[ip].dg*std::min(depthCrossSection*P[ip].Mass,M_PI*square(P[ip].Hsml));
            maxTree->getThreshList(allHotSourceP,&(P[ip].Pos[0]),receiving_area,All.TotN_gas,&localRaySourceList[localNRays],&localNThresh[localNTargets]);

            if ( localNThresh[localNTargets]>sourceChunkSize ) {
                std::cout << "Error: too many threshold particles, can't fit in local threshold packing array! " << localNThresh[localNTargets] << "/" << sourceChunkSize << std::endl;
                exit(31415);
            }
            if ( localNRays+localNThresh[localNTargets]>sourceChunkSize ) {
                break;
                // we don't increment local_particle_j etc, we'll redo this particle on the next loop
            }

            if ( localNThresh[localNTargets]>0 ) {
//                 if ( P[ip].ID ==150173 ) {
//                     std::cout << "Particle on proc " << ThisTask << " in iteration " << nits << " local ntarget " << localNTargets << " local nray offset " << localNRays << " local nthresh " << localNThresh[localNTargets] << std::endl;
//                     std::cout << "Particle location: " << P[ip].Pos[0] << " " << P[ip].Pos[1] << " " << P[ip].Pos[2] << " " << std::endl;
//                 }
//                 if ( ThisTask==0 ) {
//                     std::cout << ThisTask << " " << ip << " " << localNThresh[localNTargets] << std::endl;
//                 }
                double sumLum=0.;
                if ( ThisTask==46 && ip==1306) {
                    std::cout << "N=" << localNThresh[localNTargets] << std::endl;
                }
                for ( unsigned int i=0 ; i<localNThresh[localNTargets] ; i++ ) {
                    for ( int ic=0 ; ic<3 ; ic++ ) {
                        localRayTargetCoords[(localNRays+i)*3+ic] = P[ip].Pos[ic];
                    }
                    localRayReceivingAreas[localNRays+i] = receiving_area;
                    if ( ThisTask==46 && ip==1306) {
                        double thisLum;
                        int iSource=localRaySourceList[localNRays+i];

                        if ( iSource>=0 ) {
                            thisLum = allHotSourceP[iSource].brightness*allHotSourceP[iSource].transmitting_area;
                        } else {
                            maxlumtree *targetNode =  maxTree->nodeList->at(-iSource-1);
                            thisLum = targetNode->luminosity;
                        }
                        sumLum+=thisLum;
//                         std::cout << iSource << " " << thisLum << " " << sumLum << std::endl;;
                    }
                }
                if ( ThisTask==46 && ip==1306) {
                    std::cout << "sumLum:" << sumLum << std::endl;
//                     exit(0);
                }
                localNRays+=localNThresh[localNTargets];
                localTargetIDs[localNTargets] = ip;
                localTargetActiveIDs[localNTargets] = local_particle_j;
                localNTargets++;
    //             if (ThisTask==0 ) {
    //                 std::cout << local_particle_j << "/" << localNActive << " " << localNRays << "/" << sourceChunkSize << " " << localNTargets << std::endl;
    //             }
            }
            


            local_particle_j++;

        }
        
        if ( local_particle_j>=localNActive && !localDone ) {
            localDone = true;
            t1 = my_second();
//             printf("Proc: %d locally done. n_interactions: %d. time %g\n",ThisTask,n_interactions,timediff(t0,t1));
        } else if ( !localDone ) {
//             printf("Proc: %d. Completed %d/%d\n",ThisTask,local_particle_j,localNActive);
        }
        t_now = my_second();
        build_time+= timediff(t_then,t_now);
        t_then=t_now;

        MPI_Barrier(MPI_COMM_WORLD);
        t_now = my_second();
        wait_time1+= timediff(t_then,t_now);
        t_then=t_now;

        // We need these later for sending everything back
        int allNRays[NTask], rayNDispl[NTask], NRaysTot;
        int NSend[NTask], NDispl[NTask];
        // Stage 2 - COMMUNICATE
        {
            // Quickly check if we'll need to loop again
            MPI_Allreduce(&localDone,&allDone,1,MPI_C_BOOL,MPI_LAND,MPI_COMM_WORLD);
        
            // *** GATHER
            int *allRaySourceList;
            double *allRayAreaList;
        
            MPI_Gather(&localNRays,1,MPI_INTEGER,allNRays,1,MPI_INTEGER,0,MPI_COMM_WORLD);
            if ( ThisTask==0 ) {
                rayNDispl[0] = 0;
                for ( int iTask=0 ; iTask<NTask-1 ; iTask++ ) {
                    rayNDispl[iTask+1] = rayNDispl[iTask]+allNRays[iTask];
                }
                NRaysTot=rayNDispl[NTask-1]+allNRays[NTask-1];
                allRaySourceList = new int[NRaysTot];
                allRayAreaList = new double[NRaysTot];
                // For test
//                 if ( nits==3 ) {
//                     for ( int iTask=0 ; iTask<NTask ; iTask++ ) {
//                         std::cout << "offsets received:" << iTask << " " << allNRays[iTask] << " " << rayNDispl[iTask] << std::endl;
//                     }
//                 }
            }
    //         MPI_Barrier(MPI_COMM_WORLD);
            MPI_Gatherv(localRaySourceList,localNRays,MPI_INTEGER,allRaySourceList,allNRays,rayNDispl,MPI_INTEGER,0,MPI_COMM_WORLD);
            MPI_Gatherv(localRayReceivingAreas,localNRays,MPI_DOUBLE,allRayAreaList,allNRays,rayNDispl,MPI_DOUBLE,0,MPI_COMM_WORLD);
        
            // n.b. we are sending multiple copies of the same target particle here, so this is VERY inefficient
            int allNTargetCoord[NTask], targetCoordNDispl[NTask], NTargetCoordTot;
            double *allTargetCoord;
            if ( ThisTask==0 ) {
                for ( int iTask=0 ; iTask<NTask ; iTask++ ) {
                    targetCoordNDispl[iTask] = rayNDispl[iTask]*3;
                    allNTargetCoord[iTask] = allNRays[iTask]*3;
                }
                NTargetCoordTot=NRaysTot*3;
                allTargetCoord = new double[NTargetCoordTot];
                // For test
//                 for ( int iTask=0 ; iTask<NTask ; iTask++ ) {
//                     std::cout << iTask << " " << allNTargetCoord[iTask] << " " << targetCoordNDispl[iTask] << std::endl;
//                 }
            } 
            MPI_Gatherv(localRayTargetCoords,localNRays*3,MPI_DOUBLE,allTargetCoord,allNTargetCoord,targetCoordNDispl,MPI_DOUBLE,0,MPI_COMM_WORLD);
        
            // *** SCATTER
            // Calculate how many to send to each proc
            int NEach, NLeftover;
            int NCoordSend[NTask], NCoordDispl[NTask];

            if ( ThisTask==0 ) {
                NEach = NRaysTot/NTask;
                NLeftover = NRaysTot%NTask;
                NDispl[0] = 0;
                for ( int iTask=0 ; iTask<NTask ; iTask++ ) {
                    NSend[iTask] = NEach;
                    if ( NLeftover>0 ) {
                        NSend[iTask]++;
                        NLeftover--;
                    }
                    if ( iTask<NTask-1 ) {
                        NDispl[iTask+1] = NDispl[iTask]+NSend[iTask];
                    }
                }
//                 if ( nits==3 ) {
//                     for ( int iTask=0 ; iTask<NTask ; iTask++ ) {
//                         std::cout << "offsets sent:" << iTask << " " << NSend[iTask] << " " << NDispl[iTask] << std::endl;
//                     }
//                 }
            }
            MPI_Bcast(NSend,NTask,MPI_INT,0,MPI_COMM_WORLD);
            NtodoRays = NSend[ThisTask];
        
            // *** Communicate source IDs and target coordinates (we should pack these into a struct!)
            MPI_Scatterv(allRaySourceList,NSend,NDispl,MPI_INT,todoRaySourceList,NSend[ThisTask],MPI_INT,0,MPI_COMM_WORLD);
            MPI_Scatterv(allRayAreaList,NSend,NDispl,MPI_DOUBLE,todoRayReceivingAreas,NSend[ThisTask],MPI_DOUBLE,0,MPI_COMM_WORLD);
        
            for ( int iTask=0 ; iTask<NTask ; iTask++ ) {
                NCoordSend[iTask]=NSend[iTask]*3;
                NCoordDispl[iTask]=NDispl[iTask]*3;
            }

            MPI_Scatterv(allTargetCoord,NCoordSend,NCoordDispl,MPI_DOUBLE,todoRayTargetCoords,NCoordSend[ThisTask],MPI_DOUBLE,0,MPI_COMM_WORLD);
        
            if ( ThisTask==0 ) {
                delete[] allRaySourceList;
                delete[] allRayAreaList;
                delete[] allTargetCoord;
            }
        
        }
        
        t_now = my_second();
        comm_time1+= timediff(t_then,t_now);
        t_then=t_now;

        
        

        // Stage 3 - RAYTRACE
        {
            for ( unsigned int iray = 0 ; iray < NtodoRays ; iray++ ) {
                int iSource = todoRaySourceList[iray];
                double *targetPos = &(todoRayTargetCoords[iray*3]);
                double dist2 = get_d12_norm2(&(allHotSourceP[iSource].Pos[0]),targetPos);
//                 if ( nits==3 && ThisTask==12 && iray==30989 ) {
//                     std::cout << "found? " << targetPos[0] << " " << targetPos[1] << " " << targetPos[2] << std::endl;
//                 }
                if ( dist2<=0. ) {
                    returnDepths[iray]=-1.;
                    continue;//{ // TODO - maybe this needs to be more rigorous (checking for same p, passing indices?)
                }
                double dist=sqrt(dist2);

                // receiving_area needs to be passed too!
                double unextinctedIntensity = todoRayReceivingAreas[iray]*allHotSourceP[iSource].brightness*allHotSourceP[iSource].transmitting_area/dist2;
            
                returnDepths[iray] = one_IR_tree_ray(bigTree,
                       allHotSourceP,
                       alreadyDone, targetPos, &(allHotSourceP[iSource].Pos[0]),
                       depthCrossSection,dist,dist2,unextinctedIntensity
                       );

            
            
            }
        }
        
        t_now = my_second();
        ray_time+= timediff(t_then,t_now);
        t_then=t_now;

        
    
        // Stage 4 - bring it all back to you (bring it all back now)
        {
            double *allReturnDepths;
            if ( ThisTask==0 ) {
                allReturnDepths = new double[NRaysTot];
            }
            MPI_Barrier(MPI_COMM_WORLD);
            t_now = my_second();
            wait_time2+= timediff(t_then,t_now);
            t_then=t_now;


            MPI_Gatherv(returnDepths,NtodoRays,MPI_DOUBLE,allReturnDepths,NSend,NDispl,MPI_DOUBLE,0,MPI_COMM_WORLD);
            t_now = my_second();
            gather_time2+= timediff(t_then,t_now);
            t_then=t_now;


            MPI_Scatterv(allReturnDepths,allNRays,rayNDispl,MPI_DOUBLE,returnedDepths,localNRays,MPI_DOUBLE,0,MPI_COMM_WORLD);
            if ( ThisTask==0 ) {
                delete[] allReturnDepths;
            }
            t_now = my_second();
            scatter_time2+= timediff(t_then,t_now);
            t_then=t_now;

//             t_now = my_second();
//             comm_time2+= timediff(t_then,t_now);
//             t_then=t_now;
// 
        
//             std::cout << ThisTask << " Targets: " << localNTargets << " Rays: " << localNRays << std::endl;
            int iray = 0;
            for ( unsigned int iTarget = 0 ; iTarget<localNTargets ; iTarget++ ) {
                n_interactions++;
                int ip = localTargetIDs[iTarget];
                int j = localTargetActiveIDs[iTarget];
                
                double unextincedFluxSum=0.;

//                 if ( P[ip].ID ==150173 ) {
//                     std::cout << "STARTING RAYTRACING " << P[ip].ID << " " << localNThresh[iTarget] << std::endl;
//                 }


                for ( unsigned int sourceCount = 0 ; sourceCount<localNThresh[iTarget] ; sourceCount++ ) {
                    int iSource = localRaySourceList[iray];
//                     if ( P[ip].ID ==150173 && sourceCount==873  ) {
//                         std::cout << "RAY " << sourceCount  << " " << iSource << std::endl;
//                         std::cout << "TRACK: " << ThisTask << " " << iTarget  << " " << sourceCount << " " << iray << std::endl;
//                     }
//                     if ( P[ip].ID ==150173 && sourceCount==873 ) {
//                         std::cout << "depth " << returnedDepths[iray] << std::endl;
//                     }
                    /// THIS GOES HERE, ONLY MOVED TO BELOW FOR TEST
//                     if ( isinf(returnedDepths[iray]) || returnedDepths[iray]<0. ) {
//                         iray++;
//                         continue;
//                     }
                    double *sourceCoord,sourceLuminosity;
                    if ( iSource>=0 ) {
                        sourceCoord = &(allHotSourceP[iSource].Pos[0]);
                        sourceLuminosity = allHotSourceP[iSource].brightness*allHotSourceP[iSource].transmitting_area;
                    } else {
                        maxlumtree *targetNode =  maxTree->nodeList->at(-iSource-1);
                        sourceCoord = &(targetNode->r_cent[0]);
                        sourceLuminosity = targetNode->luminosity;
                    }
                    // nb we are calculating unextinctedIntensity and dist twice!
                    double dist2 = get_d12_norm2(sourceCoord,&(P[ip].Pos[0]));
//                     if ( P[ip].ID ==150173 && sourceCount==873 ) {
//                         std::cout << "dist2 " << dist2 << std::endl;
//                     }
                    double dist = sqrt(dist2);
                    // FOR TEST - this isn't needed when we move the isinf test back up
                    if ( dist<=0. ) {
                        iray++;
                        continue;
                    }
                    double unextinctedIntensity = localRayReceivingAreas[iray]*sourceLuminosity/dist2;
                    if ( ThisTask==46 && ip==1306 ) {
                        unextincedFluxSum+=unextinctedIntensity;
                    }
                    if ( isinf(returnedDepths[iray]) || returnedDepths[iray]<0. ) {
                        iray++;
                        continue;
                    }

                    // Actually add the force and absorption
                    double rad_received = unextinctedIntensity/exp(returnedDepths[iray]);
                    rad_received*=1.e28; // BOOST FOR TEST
                    IR_absorbed[j] += rad_received;
//                     IR_absorbed[j] += 1.;
                    
//                     if ( IR_absorbed[j]>50. ) {
//                         std::cout << ThisTask << " " << iTarget << " " << j << " " << IR_absorbed[j] << std::endl;
//                     }
                    
//                     if ( P[ip].ID ==150173 && sourceCount==873 ) {
//                         std::cout << "RAD " << sourceCount  << " " << iSource << " " << rad_received << std::endl;
//                     }

                    double force_rad_received_normed = rad_received/(C*All.UnitVelocity_in_cm_per_s)/dist;
                    for ( int ic=0 ; ic<3 ; ic++ ) {
                        IR_force[j*3+ic] += force_rad_received_normed*(P[ip].Pos[ic]-sourceCoord[ic]);
                    }

                    iray++;
                }
                if ( ThisTask==46 && ip==1306 ) {
                    std::cout << "sums=" << unextincedFluxSum << " " << IR_absorbed[j] << std::endl;
                    exit(0);
                }
//                 if ( IR_absorbed[j]>0. ) {
//                     std::cout << ThisTask << " " << ip << " " << j << " " << IR_absorbed[j] << std::endl;
//                 }

            }
//             MPI_Barrier(MPI_COMM_WORLD);
//             exit(0);
            
            t_now = my_second();
            force_time+= timediff(t_then,t_now);
            t_then=t_now;

        }
//         if ( ThisTask==0 ) {
//             std::cout << "ITERATION COMPLETE" << nits << std::endl;
//         }
//         std::cout << ThisTask << " " << IR_absorbed[0] << std::endl;
//         if ( ThisTask==46 ) {
//             int iTarget = 3;
//             int j = localTargetActiveIDs[iTarget];
//             std::cout << ThisTask << " " << iTarget << " " << localNThresh[iTarget] << " " << IR_force[j*3] << " " << IR_force[j*3+1] << " " << IR_force[j*3+2] << std::endl;
//             j = localTargetActiveIDs[iTarget];
//             iTarget = 18;
//             std::cout << ThisTask << " " << iTarget << " " << localNThresh[iTarget] << " " << IR_force[j*3] << " " << IR_force[j*3+1] << " " << IR_force[j*3+2] << std::endl;
//         }

        nits++;
    }

    // DELETE THINGS
    delete[] localRaySourceList;
    delete[] localRayTargetCoords;
    delete[] localRayReceivingAreas;
    delete[] localTargetIDs;
    delete[] localTargetActiveIDs;
    delete[] todoRaySourceList;
    delete[] todoRayTargetCoords;
    delete[] todoRayReceivingAreas;
    delete[] returnDepths;
    delete[] returnedDepths;
    
    
    
    
    
    //

//     int *threshList = new int[All.TotN_gas];
//     
//     unsigned int n_interactions = 0;
// 
//     for ( j=0 ; j<localNActive ; j++ ) {
//         int ip = localIndex[j];
//         
//         double receiving_area = SphP[ip].dg*std::min(depthCrossSection*P[ip].Mass,M_PI*square(P[ip].Hsml));
//         maxTree->getThreshList(allHotSourceP,&(P[ip].Pos[0]),receiving_area,All.TotN_gas,threshList,&nThresh);
// 
//         if ( nThresh<=0 ) {
//             continue;
//         }
// 
//         for ( jp=0 ; jp<nThresh ; jp++ ) {
//             n_interactions+=1;
//             i = threshList[jp];
//             double dist2 = get_d12_norm2(&(allHotSourceP[i].Pos[0]),&(P[ip].Pos[0]));
//             if ( dist2<=0. ) continue;//{ // TODO - maybe this needs to be more rigorous (checking for same p, passing indices?)
// //                     double depthCrossSection = dustDustAbsorbT/square(allHotSourceP[i*sourceP_Ndata+3]);
//             double dist=sqrt(dist2);
// 
//             double unextinctedIntensity = receiving_area*allHotSourceP[i].brightness*allHotSourceP[i].transmitting_area/dist2;
//             double depth = one_IR_tree_ray(bigTree,
//                    allHotSourceP,
//                    alreadyDone, j, &(allHotSourceP[i].Pos[0]),
//                    localOffset,localNActive,localIndex,
//                    localHotSourceOffset,allHotSourceIndex,
//                    depthCrossSection,dist,dist2,unextinctedIntensity
//                    );
//             if ( isinf(depth) ) continue;
//             double rad_received = unextinctedIntensity/exp(depth);
//             rad_received*=1.e28; // BOOST FOR TEST
//             IR_absorbed[j] += rad_received;
//             double force_rad_received_normed = rad_received/(C*All.UnitVelocity_in_cm_per_s)/dist;
//             for ( ic=0 ; ic<3 ; ic++ ) {
//                 IR_force[j*3+ic] += force_rad_received_normed*(P[ip].Pos[ic]-allHotSourceP[i].Pos[ic]);
//             }
// 
//         }
// 
// 
//     }
//     delete[] threshList;

    t1 = my_second();
//     double t_tot = timediff(t0,t1); // for percent
    double times[] = {timediff(t0,t1),build_time,wait_time1,comm_time1,ray_time,wait_time2,gather_time2,scatter_time2,force_time};
    unsigned int nActiveTot,n_interactionsTot;

    MPI_Reduce(&localNActive,&nActiveTot,1,MPI_UNSIGNED,MPI_SUM,0,MPI_COMM_WORLD);
    MPI_Reduce(&n_interactions,&n_interactionsTot,1,MPI_UNSIGNED,MPI_SUM,0,MPI_COMM_WORLD);
    if ( ThisTask==0 ) {
        std::cout << NTask << " " << sourceChunkSize << " " << nActiveTot << " " << n_interactionsTot << " ";
    }
    for ( int itime=0 ; itime<ntimes ; itime++ ) {
        double this_time;

        MPI_Reduce(&(times[itime]),&this_time,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
        if ( ThisTask==0 ) {
            std::cout << this_time/NTask << " ";
        }
    }
    if ( ThisTask==0 ) {
        std::cout << std::endl;
    }
//     printf("Proc: %d n_int: %d. time: %g build: %g wait1: %g comm1: %g ray: %g wait2: %g gather2: %g scatter2: %g force: %g\n",ThisTask,n_interactions,timediff(t0,t1),
//             build_time/t_tot,wait_time1/t_tot,comm_time1/t_tot,ray_time/t_tot,wait_time2/t_tot,gather_time2/t_tot,scatter_time2/t_tot,force_time/t_tot);
    
    std::ofstream f;
    f.open("rad_dump"+std::to_string(ThisTask)+".dat");
    for ( unsigned int i=0 ; i<NumPart ; i++ ) {
        f << P[i].ID << " " << P[i].Pos[0] << " " << P[i].Pos[1] << " " << P[i].Pos[2] << " " << IR_absorbed[i] << " " << IR_force[i*3] << " " << IR_force[i*3+1] << " " << IR_force[i*3+2] << std::endl;
    }
    f.close();

    
    MPI_Barrier(MPI_COMM_WORLD);
//     }} // End of testloop
    exit(0);
    
    // LOTS OF DELETES TO DO
    
    return std::make_tuple(IR_absorbed,IR_force);

}

double one_IR_tree_ray(std::shared_ptr<absorbtree> tree,
                    // double AllPos[],
                       struct SourceP_Type allHotSourceP[],
                       bool alreadyDone[],double rPart[], double r_source[],
                       double depthCrossSection, double d12_norm,double d12_norm2, double unextinctedIntensity) {

    

    unsigned int lolsize = 0;
    tree->beam(r_source,rPart,lol,&lolsize,ThisTask);
    
    memset(alreadyDone, 0, sizeof(bool)*All.TotN_gas );
    
    double depth = 0.;
    
    if ( lolsize>maxlolsize ) {
        endrun(8675309);
    }
    
    for ( unsigned int ilist=0 ; ilist<lolsize ; ilist++ ) {
        std::vector<int > *hitp_set = lol[ilist];
        unsigned int hs = hitp_set->size() ;
        for ( unsigned int ii=0 ; ii<hs ; ii++ ) {
            int kg = (*hitp_set)[ii];
            
            if ( kg>=All.TotN_gas || kg<0 ) {
                std::cout << "bad kg" << std::endl;
                exit(0);
            }

            if ( alreadyDone[kg] ) continue;
            alreadyDone[kg] = true;

            
            double *rAbsorb = &(allHotSourceP[kg].Pos[0]);

            int bet = between_particles(r_source,rPart,rAbsorb,allHotSourceP[kg].Hsml,d12_norm);
            if ( bet==0 ) continue;
            
            double d2int = intersect_d2_nonorm(r_source,rPart,rAbsorb)/d12_norm2;
            double h2 = square(allHotSourceP[kg].Hsml);


            // within line of sight
            if ( d2int>h2 ) continue;

            if ( bet==1 ) {
                //extincting particle is fully between the end points, can use simper method
                depth+=allHotSourceP[kg].Mass*agn_kernel->flat_w2(d2int,h2)*depthCrossSection;

                if ( depth>IR_depth_cutoff ) {
                    // depth is huge - we don't need to calculate it any further, because heating & rad pressure
                    // from this particle is now negligible
                    if ( unextinctedIntensity*exp(-depth)<=IR_strength_cutoff ) {
                        return std::numeric_limits<double>::infinity();
                    }
                }
            } else {

            // calculate line of sight
                double z3=0.;

                double source_overlap,target_overlap;

                for ( int jj=0 ; jj<3 ; jj++ ) {
                    z3+=(rAbsorb[jj]-r_source[jj])*(rPart[jj]-r_source[jj]);
                }

                z3/=d12_norm;
                source_overlap = z3/allHotSourceP[kg].Hsml;
                if ( source_overlap>1. ) source_overlap=1.;
                target_overlap = (d12_norm-z3)/allHotSourceP[kg].Hsml;
                if ( target_overlap>1. ) target_overlap=1.;

                double weight_add = agn_kernel->half_flat_w2_truncated(d2int,h2,source_overlap)+agn_kernel->half_flat_w2_truncated(d2int,h2,target_overlap);
                if ( weight_add>0. ) {
                    depth+=allHotSourceP[kg].Mass*weight_add*depthCrossSection;
                }

                if ( depth>IR_depth_cutoff ) {
                    // depth is huge - we don't need to calculate it any further, because heating & rad pressure
                    // from this particle is now negligible
                    if ( unextinctedIntensity*exp(-depth)<=IR_strength_cutoff ) {
                        return std::numeric_limits<double>::infinity();
                    }
                }
            }
            
        }
    }
    
    return depth;
}



// Calculate dust-dust radiation
// ~~~use same allpos table to find all *destination* particles~~~ - don't use allpos, it incorporates skin depth from centre!!

// but need to transmit all of my *source* particles (positions and temperatures to everyone - which may only include "hot" particles
// also need to calculate dust/gas temperature maybe?
// Then do all rays between all source and all destination particles through all absorbing particles
// Using placeholder opacity = (10 cm^2/g) (T/10 K)**-2 where T is the blackbody source
// use this for both absorption and heating/radiation
void do_dust_dust_heating(
//             double AllPos[],
            bool alreadyDone[], int localOffset, int localNActive, int localIndex[], int nActiveTot, double *tdiffwait,double *tdifftree,double *tdiffray) {
    localHotSourceP = new SourceP_Type[N_gas];
    allHotSourceP = new SourceP_Type[All.TotN_gas];
    allHotSourceIndex = new int[All.TotN_gas];

    // timing
    double t0,t1;
//     static int times_run = 0;


    // tree of *all* extincting particles from all procs
    std::shared_ptr<absorbtree> bigTree = nullptr;
    std::shared_ptr<maxlumtree> maxTree = nullptr;
    int i,ic;
    
    unsigned int jp;
    
    //double heatEmit=5.67; // erg/s/g/K**2 - estimate of energy absorption from a blackbody (involves both terms, in notebook 16/6/17)
    //heatEmit*=All.UnitMass_in_g/All.UnitEnergy_in_cgs*All.UnitTime_in_s; // energy/time/mass/(temperature of emitter in K)**2
    //double dustDustAccelFactor = heatEmit/(C/All.UnitVelocity_in_cm_per_s);
    
//     const double molecular_mass = 4.0 / (1 + 3 * HYDROGEN_MASSFRAC);	/* note: assuming NEUTRAL GAS */
//     const double K_TO_U = All.UnitMass_in_g / All.UnitEnergy_in_cgs*BOLTZMANN/GAMMA_MINUS1/(molecular_mass*PROTONMASS);

    (*tdifftree)=0.;
    (*tdiffwait)=0.;
    (*tdiffray)=0.;

    t0 = WallclockTime = my_second();

    dust_dust_packgather_hot();
    
    if ( ThisTask==0 ) {
        std::cout << ThisTask << " STARTED DUST_DUST" << std::endl;
        std::cout << "ngas:" << All.TotN_gas << " nActiveTot:" << nActiveTot << std::endl;
    }

    // build minttemp tree
    // store: All.heatEmit*square(allHotSourceP[i*5+3])*allHotSourceP[i*5+4]/M_PI as "temperature" to minimise in table
    std::tie(maxTree,bigTree) = dust_dust_buildtrees();

//     if ( ThisTask==0 ) std::cout << "B mintree " << mintree.use_count() << " bigTree " << bigTree.use_count() << std::endl; 


    t1 = WallclockTime = my_second();
    (*tdifftree) += timediff(t0,t1);

//     t0 = WallclockTime = my_second();
//     MPI_Barrier(MPI_COMM_WORLD);
//     t1 = WallclockTime = my_second();
//     (*tdiffwait)+=timediff(t0,t1);

    t0 = WallclockTime = my_second();

    double *IR_absorbed,*IR_force;

    std::tie(IR_absorbed,IR_force) = dust_dust_rays(localNActive,maxTree,bigTree,localOffset,localIndex,alreadyDone);
    
    delete[] allHotSourceIndex;
    delete[] localHotSourceP;
    delete[] allHotSourceP;


    t1 = WallclockTime = my_second();
    (*tdiffray)+=timediff(t0,t1);

    t0 = WallclockTime = my_second();
    MPI_Barrier(MPI_COMM_WORLD);
    t1 = WallclockTime = my_second();
    (*tdiffwait)+=timediff(t0,t1);
    
    jp = 0;
    for(i = 0; i < NumPart; i++) {
        if( P[i].Type==0 && P[i].Mass > 0 && TimeBinActive[P[i].TimeBin] ) {
            SphP[i].DtEnergyIR = IR_absorbed[jp];
            for ( ic=0 ; ic<3 ; ic++ ) {
                SphP[i].IRRadAccel[ic] = IR_force[jp*3+ic];
            }
            jp++;
        }
    }
    
    double totalTime;
    MPI_Allreduce(tdifftree, &totalTime, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    if ( ThisTask==0 ) std::cout << "tdifftree: " << totalTime << std::endl;
    MPI_Allreduce(tdiffwait, &totalTime, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    if ( ThisTask==0 ) std::cout << "tdiffwait: " << totalTime << std::endl;
    MPI_Allreduce(tdiffray, &totalTime, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    if ( ThisTask==0 ) std::cout << "tdiffray: " << totalTime << std::endl;

    // dump data - debuggering
//    std::cout << ThisTask << " DUMPING" << std::endl;
    /*std::ofstream fout;
    fout.open("threshdump"+std::to_string(ThisTask)+".dat");
    jp = 0;
    for(i = 0; i < NumPart; i++) {
        if( P[i].Type==0 && P[i].Mass > 0 && TimeBinActive[P[i].TimeBin] ) {
            fout << P[i].Pos[0] << " " <<P[i].Pos[1] << " " <<P[i].Pos[2] << " " << IR_absorbed[jp] << " " << SphP[i].InternalEnergy << std::endl;
            jp++;
        }
    }
    fout.close();*/

    
    delete[] IR_absorbed;
    delete[] IR_force;


//     MPI_Barrier(MPI_COMM_WORLD);
    
//     if ( ThisTask==0 ) std::cout << "D mintree " << mintree.use_count() << " bigTree " << bigTree.use_count() << std::endl; 
    if ( ThisTask==0 ) {
        std::cout << ThisTask << " DONE DUST_DUST!" << std::endl;
//         times_run++;
//         std::cout << "nrun=" << times_run << std::endl;
//         if ( times_run==2 )
//             exit(0);
    }
    
    
//     exit(0);
}
