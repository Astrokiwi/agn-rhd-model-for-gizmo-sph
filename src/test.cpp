#include <math.h>
#include <stdio.h>
#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include "allvars.h"
extern "C"
{
#include "proto.h"
}
#include "hydro/cpp_prototypes.h"
#include "hydro/cpp_helpers.h"


extern "C"
{
void initial_tests();
void ics_loaded_tests();
}


double minimum_accuracy = 1.e-9;

double norm(double* a) {
    return sqrt(square(a[0])+square(a[1])+square(a[2]));
}

inline bool near_equal(double a,double b) {
    double x = (a-b)/a;
    return x<=minimum_accuracy && x>=-minimum_accuracy;
}


inline bool near_zero(double x) {
    return x<=minimum_accuracy && x>=-minimum_accuracy;
}

double product_norm(double* a,double* b,double *c) {
    double sum = norm(a)*norm(b)-norm(c);
    return near_zero(sum);
}

double normalised_dot_product(double* a, double *b) {
    double x=0;
    int ii;
    for ( ii=0 ; ii<3 ; ii++ ) {
        x+=a[ii]*b[ii];
    }
    x/=norm(a)*norm(b);
    return x;
}

void print_vec(double *a) {
    printf("%g %g %g\n",a[0],a[1],a[2]);
}

bool orthogonal(double* a, double *b) {
    return normalised_dot_product(a,b)<=minimum_accuracy;
}

bool opposing(double* a, double *b) {
    double sum = 0;
    int ii;
    for ( ii=0 ; ii<3 ; ii++ ) {
        sum+=a[ii]+b[ii];
    }
    
    return near_zero(sum);
}

bool vec_near_zero(double *a) {
    double sum = 0;
    int ii;
    for ( ii=0 ; ii<3 ; ii++ ) {
        sum+=a[ii];
    }
    
    return near_zero(sum);
}

bool vec_equal(double* a, double *b) {
    double sum = 0;
    double normalisation = norm(a)+norm(b);
    int ii;
    for ( ii=0 ; ii<3 ; ii++ ) {
        sum+=a[ii]-b[ii];
    }
    
    if ( normalisation>0. ) {
        sum/=normalisation;
    }
    
    return sum<=minimum_accuracy && sum>=-minimum_accuracy;
}

bool r2d_equal(double r1_x,double r1_y,double r2_x,double r2_y) {
    double sum = r1_x-r2_x + r1_y-r2_y;
    return near_zero(sum);
}


void test_cross_product() {
    // tests:
    // 1. parallel vectors -> 0
    // 2. orthogonal vectors -> result is orthogonal to both, magnitude is product
    // 3. arbitrary vectors -> result is orthogonal to both
    // 4. anticommutative property
    // 5. cross product of unit vectors (in right order) gives next unit vector

    double result[3],result2[3];
    
    int ii;
    
    int iaxis,jaxis,kaxis;
    for ( iaxis=0 ; iaxis<3 ; iaxis ++ ) {

        // unit vectors
        double unit_1[3] = {0,0,0};
        double unit_2[3] = {0,0,0};
        double unit_3[3] = {0,0,0};
        jaxis = (iaxis+1)%3;
        kaxis = (jaxis+1)%3;
        
        unit_1[iaxis]=1;
        unit_2[jaxis]=1;
        unit_3[kaxis]=1;

        cross_product(unit_1,unit_2,result);
        assert(orthogonal(unit_1,result));
        assert(orthogonal(unit_2,result));
        assert(vec_equal(unit_3,result));
        

        cross_product(unit_2,unit_1,result2);
        assert(orthogonal(unit_1,result2));
        assert(orthogonal(unit_2,result2));
        assert(opposing(result,result2));

        cross_product(unit_1,unit_1,result);
        assert(vec_near_zero(result));


        // axis-aligned vectors of random length
        unit_1[iaxis]=(double)rand()/RAND_MAX*1000.-500.;;
        unit_2[jaxis]=(double)rand()/RAND_MAX*1000.-500.;;
        cross_product(unit_1,unit_2,result);
        assert(orthogonal(unit_1,result));
        assert(orthogonal(unit_2,result));
        assert(product_norm(unit_1,unit_2,result));
        
        // totally random vectors
        for ( ii=0 ; ii<3 ; ii++ ) {
            unit_1[ii] = (double)rand()/RAND_MAX*1000.-500.;
            unit_2[ii] = (double)rand()/RAND_MAX*1000.-500.;
        }
        cross_product(unit_1,unit_2,result);
        assert(orthogonal(unit_1,result));
        assert(orthogonal(unit_2,result));

        cross_product(unit_2,unit_1,result2);
        assert(opposing(result,result2));
        

    }
    
}

void test_binary_orbit() {
    // tests:
    // circular orbit is equal to sin/cos
    // elliptical orbits have correct period
    // all orbits are clockwise
    
    // G is not set yet in initial tests, we use G=1 units
    All.G = 1.;

    minimum_accuracy=1.e-9;
    
    StellarBinary circ_orbit(       1. // M1
                                    ,1. // M2
                                    ,0. // eccentricity
                                    ,1. // distance
    );
    
    
    double circ_freq = sqrt(All.G*2.);
    
    for ( int ii=0 ; ii<100 ; ii++ ) {
        //double t = (double)rand()/RAND_MAX/circ_freq*10.;
        double t = ii*0.01;
        double x = 0.5*sin(t*circ_freq), y = 0.5*cos(t*circ_freq);
        circ_orbit.update(t);
//        printf("%g %g %g %g %g %g\n",t,t*circ_freq,x,y,circ_orbit.r_1[0],circ_orbit.r_1[1]);
//        printf("%g %g %g %g\n",circ_orbit.semi_major_axis_1,circ_orbit.semi_minor_axis_1,circ_orbit.semi_major_axis_2,circ_orbit.semi_minor_axis_2);
//        printf("%g %g\n",circ_freq,circ_orbit.angular_freq);
        assert(r2d_equal(x,y,circ_orbit.r_1[0],circ_orbit.r_1[1]));
        assert(r2d_equal(-x,-y,circ_orbit.r_2[0],circ_orbit.r_2[1]));
    }

    
    minimum_accuracy=1.e-3;

    // eccentric orbit
    for ( int jj=0 ; jj<10 ; jj++ ) {
        StellarBinary ecc_orbit(       (double)rand()/RAND_MAX*10. // M1
                                        ,(double)rand()/RAND_MAX*10. // M2
                                        ,(double)rand()/RAND_MAX*.8+.1 // eccentricity
                                        ,(double)rand()/RAND_MAX*10. // distance
        );


        double ecc_period = 2.*M_PI/ecc_orbit.angular_freq;

        for ( int ii=0 ; ii<100 ; ii++ ) {
            double t0 = (double)rand()/RAND_MAX*ecc_period*10.;
            double t1 = t0+ecc_period*ii;
            ecc_orbit.update(t0);
            double x = ecc_orbit.r_1[0], y = ecc_orbit.r_1[1];
            double x2 = ecc_orbit.r_2[0], y2 = ecc_orbit.r_2[1];
            ecc_orbit.update(t1);
            assert(r2d_equal(x,y,ecc_orbit.r_1[0],ecc_orbit.r_1[1]));
            assert(r2d_equal(x2,y2,ecc_orbit.r_2[0],ecc_orbit.r_2[1]));
        }
    }

    minimum_accuracy=1.e-9;
    
}

void initial_tests() {
    test_cross_product();
    test_binary_orbit();
}

void reset_accretion() {
    for ( int i=0 ; i<3 ; i++ ) {
        All.BH_binary.accreted_J_1[i]=0.;
        All.BH_binary.accreted_J_2[i]=0.;
        All.BH_binary.accreted_momentum_1[i] = 0.;
        All.BH_binary.accreted_momentum_2[i] = 0.;
    }
    
    All.BH_binary.accreted_mass_1 = 0.;
    All.BH_binary.accreted_mass_2 = 0.;
}

void test_delete_all() {
    minimum_accuracy = 1.e-4;
    // Create arbitrary particle and SMBH distributions, check if angular momentum is measured right
    reset_accretion();

    // All particles at one point, SMBHs at origin
    for ( int i = 0 ; i<NumPart ; i++ ) {
        P[i].Mass = 7.;
        P[i].Pos[0] = 2;P[i].Pos[1] = 0;P[i].Pos[2] = 0;
        P[i].Vel[0] = 3;P[i].Vel[1] = 4;P[i].Vel[2] = 0;
    }
    
    double j_estimate = 8.*All.TotN_gas*P[0].Mass;
    double p_estimate = 5.*All.TotN_gas*P[0].Mass;
    
    All.innerKillRad = 4.;
    
    for ( int j = 0 ; j<3 ; j++ ) {
        All.BH_binary.Pos_1[0] = 0.;
        All.BH_binary.Pos_2[0] = 0.;
    }
    
    update_deathzone();
    
    if ( ThisTask==0 ) {
        // should mark everything for deletion, all taken by 1st SMBH
        assert(vec_near_zero(All.BH_binary.accreted_momentum_2));
        assert(vec_near_zero(All.BH_binary.accreted_J_2));
        
        assert(near_equal(norm(All.BH_binary.accreted_momentum_1),p_estimate));
        assert(near_zero(All.BH_binary.accreted_J_1[0]));
        assert(near_zero(All.BH_binary.accreted_J_1[1]));
        assert(near_equal(j_estimate,All.BH_binary.accreted_J_1[2]));
        
    }
    
    reset_accretion();
    // All particles at origin, SMBHs at one point, should give exact same values
    for ( int i = 0 ; i<NumPart ; i++ ) {
        P[i].Mass = 7.;
        P[i].Pos[0] = 1;P[i].Pos[1] = 0;P[i].Pos[2] = 0;
        P[i].Vel[0] = 3;P[i].Vel[1] = 4;P[i].Vel[2] = 0;
    }
    All.BH_binary.Pos_1[0] = -1.;
    All.BH_binary.Pos_2[0] = -1.;
    
    j_estimate = 4.*All.TotN_gas*P[0].Mass;

    update_deathzone();
    
    if ( ThisTask==0 ) {
        // should mark everything for deletion, all taken by 1st SMBH
        assert(vec_near_zero(All.BH_binary.accreted_momentum_2));
        assert(vec_near_zero(All.BH_binary.accreted_J_2));
        
        assert(near_equal(norm(All.BH_binary.accreted_momentum_1),p_estimate));
        assert(near_zero(All.BH_binary.accreted_J_1[0]));
        assert(near_zero(All.BH_binary.accreted_J_1[1]));
        assert(near_equal(All.BH_binary.accreted_J_1[2],j_estimate));
        
    }


    reset_accretion();
    // Particles in two piles
    for ( int i = 0 ; i<NumPart ; i++ ) {
        P[i].Mass = 7.;
    }
    All.BH_binary.Pos_1[0] = -2.;
    All.BH_binary.Pos_1[1] = -3.;
    All.BH_binary.Pos_1[2] = -4.;

    All.BH_binary.Pos_2[0] = 2.;
    All.BH_binary.Pos_2[1] = 3.;
    All.BH_binary.Pos_2[2] = 4.;

    for ( int i = 0 ; i<NumPart ; i+=2 ) {
        P[i].Pos[0] = -3;P[i].Pos[1] = -4;P[i].Pos[2] = -5;
        P[i].Vel[0] = 1;P[i].Vel[1] = 2;P[i].Vel[2] = 3;
    }
    double j_estimate_1[3] = {-2,4,-2};
    double p_estimate_1[3];
    for ( int i = 1 ; i<NumPart ; i+=2 ) {
        P[i].Pos[0] = 5;P[i].Pos[1] = 3;P[i].Pos[2] = 5;
        P[i].Vel[0] = 2;P[i].Vel[1] = 1;P[i].Vel[2] = 3;
    }
    double j_estimate_2[3] = {4,-5,-1};
    double p_estimate_2[3];

    for ( int j = 0 ; j<3 ; j++ ) {
        j_estimate_1[j]*=P[0].Mass*All.TotN_gas/2;
        j_estimate_2[j]*=P[0].Mass*All.TotN_gas/2;
        
        p_estimate_1[j]=P[0].Vel[j]*All.TotN_gas*P[0].Mass/2.;
        p_estimate_2[j]=P[1].Vel[j]*All.TotN_gas*P[0].Mass/2.;
    }
    
    update_deathzone();
    
    if ( ThisTask==0 ) {
        // should mark everything for deletion, all taken by 1st SMBH
        assert(vec_equal(All.BH_binary.accreted_momentum_1,p_estimate_1));
        assert(vec_equal(All.BH_binary.accreted_momentum_2,p_estimate_2));
        assert(vec_equal(All.BH_binary.accreted_J_1,j_estimate_1));
        assert(vec_equal(All.BH_binary.accreted_J_2,j_estimate_2));
        
    }


    minimum_accuracy = 1.e-9;
    
}

void ics_loaded_tests() {
    test_delete_all();
}