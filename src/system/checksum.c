#include <mpi.h>
#include <stdlib.h>

#include "../allvars.h"
#include "../proto.h"

void agn_checksum() {
    
    double local_checksum  = 0.;
    double global_checksum = 0.;
    int i,j;
    
    for ( i=0 ; i<NumPart ; i++ ) {
        for ( j=0 ; j<3 ; j++ ) {
            local_checksum+=SphP[i].RadAccel[j];
        }
    }

    MPI_Allreduce(&(local_checksum), &global_checksum, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    if (ThisTask==0) {
        printf("`mpirun -np 8 ./GIZMO test_0m0001.param` checksum'\n");
        printf("GLOBAL      CHECKSUM: %32.7f              46629897800.7162552\n",global_checksum);
        printf("LOCAL(root) CHECKSUM: %32.7f              13798492388.1159821\n",local_checksum);
        printf("EXITING\n");
    }


    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
    exit(0);            
}